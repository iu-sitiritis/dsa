package edu.innopolis;

/**
 * <p>
 *  The interface represents a correction method used to change a character in word, so after the correction a
 *  source word matches a target.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 * @see DoNothingCorrectionMethod
 * @see AddCorrectionMethod
 * @see DeleteCorrectionMethod
 * @see SubstitutionCorrectionMethod
 * @see TranspositionCorrectionMethod
 */
public interface ICorrectionMethod
{
  /**
   * @return correctionCost
   */
  default double getCorrectionCost()
  {
    return correctionCost;
  }

  /**
   * <p>
   *   Based on state of dynamicMatrix, srcMatrixI, trgMatrixI, sourceWord and targetWord calculates a correction cost
   *   for the dynamicMatrix[srcMatrixI][trgMatrixI].
   * </p>
   *
   * @param dynamicMatrix Array used by dynamic algorithm to calculate the cost of correcting a source word to transform
   * it into a target word. The matrix is of size [sourceWord.length + 1][targetWord.length + 1]. An array is chosen,
   * since the size of the matrix is known and not changed, once it is created and it guarantees O(1) access by index.
   * @param sourceWord Word that is to be transformed into the targetWord
   * @param targetWord Word, sourceWord is transformed into
   * @param srcMatrixI Current row index of the dynamicMatrix
   * @param trgMatrixI Current column index of the dynamicMatrix
   * @exception IndexOutOfBoundsException If srcI or trgI is invalid indexes the exception is thrown.
   * @return Number of corrections needed to transform sourceWord[0..srcMatrixI - 1] into targetWord[0..targetWord - 1],
   * based on dynamicMatrix calculated so far, srcI, trgI and the correction method's cost.
   */
  double calculateCorrectionCost(
    double dynamicMatrix[][],
    String sourceWord,
    String targetWord,
    int srcMatrixI, int trgMatrixI
  ) throws IndexOutOfBoundsException;

  /**
   * <p>
   *   Cost of the implemented correction method. Determines how similar words are. The greater the value, the less
   *   the correction method's efficiency and the more words are different to each other in case of picking the
   *   correction method.
   *   The condition must be satisfied: correctionCost > 0.
   * </p>
   */
  double correctionCost = 1.0;
}
