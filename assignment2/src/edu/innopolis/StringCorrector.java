package edu.innopolis;

import java.util.*;

/**
 * <p>
 *   A static class that contains static methods for calculating an edit distance between 2 words and based on a given
 *   dictionary providing the closest words to a given word.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 */
public final class StringCorrector
{
  private StringCorrector() {}

  /**
   * <p>
   *   Calculates an edit distance between sourceWord and targetWord, based on defined set of possible operations that
   *   transform sourceWord into targetWord with a specified cost for each transformation.
   *   The operations are defined by correctionMethods. AddCorrectionMethod and DeleteCorrectionMethod are 2 basic
   *   operations. correctionMethods should specify other correction methods in addition to AddCorrectionMethod and
   *   DeleteCorrectionMethod. The algorithm implements the idea of a
   *   <a href="https://en.wikipedia.org/wiki/Wagner%E2%80%93Fischer_algorithm">
   *     Wagner–Fischer algorithm
   *   </a>, that uses a 2D
   *   <a href="https://en.wikipedia.org/wiki/Dynamic_programming">
   *     dynamic programming strategy
   *   </a>.
   * </p>
   *
   * @param sourceWord A word for which an edit distance to targetWord is calculated.
   * @param targetWord A word for which an edit distance from sourceWord is calculated.
   * @param correctionMethods Defines a set of operations that defines correction methods that can be used to transform
   *   sourceWord into targetWord along with the cost of these operations. 2 basic operations that are already a part
   *   of the algorithm are: AddCorrectionMethod and DeleteCorrectionMethod. Iterable<> is chosen, since each given
   *   method must calculate a cost associated with it while performing the dynamic.
   * @param addCorrectionMethodCost Cost of the AddCorrectionMethod.
   * @param deleteCorrectionMethodCost Cost of the DeleteCorrectionMethod.
   * @return Edit distance, based on specified correctionMethods. If no correction methods were provided
   * (i. e. correctionMethods == null || correctionMethods.size() == 0), calculates
   * <a href="https://en.wikipedia.org/wiki/Longest_common_subsequence_problem">
   *   Longest common subsequence (LCS) distance
   * </a>.
   * If correctionMethods contains SubstitutionCorrectionMethod - the returned edit distance is a
   * <a href="https://en.wikipedia.org/wiki/Levenshtein_distance">
   *   Levenshtein distance
   * </a>.
   * If in addition to SubstitutionCorrectionMethod it also contains TranspositionCorrectionMethod - the returned edit
   * distance is a
   * <a href="https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance">
   *   Damerau–Levenshtein distance
   * </a>.
   */
  public static double estimate(
    String sourceWord,
    String targetWord,
    Iterable<ICorrectionMethod> correctionMethods,
    Optional<Integer> addCorrectionMethodCost,
    Optional<Integer> deleteCorrectionMethodCost
  )
  {
    AddCorrectionMethod additionMethod = new AddCorrectionMethod(
      (addCorrectionMethodCost.isPresent()) ? addCorrectionMethodCost.get() : 1
    );
    DeleteCorrectionMethod deletionMethod = new DeleteCorrectionMethod(
      (deleteCorrectionMethodCost.isPresent()) ? deleteCorrectionMethodCost.get() : 1
    );
    DoNothingCorrectionMethod doNothingMethod = new DoNothingCorrectionMethod();
    int numMatrixRows = sourceWord.length() + 1;
    int numMatrixColumns = targetWord.length() + 1;
    double dynamicMatrix[][] = new double[numMatrixRows][numMatrixColumns];

    // initialize first column, that is number of additions needed to transform an empty string into a targetWord[0..i]
    for (int i = 0; i < numMatrixRows; ++i)
    {
      dynamicMatrix[i][0] = i;
    }

    // initialize first row, that is number of deletions needed to transform a sourceWord[0..i] into an empty string
    for (int i = 0; i < numMatrixColumns; i++)
    {
      dynamicMatrix[0][i] = i;
    }

    for (int i = 1; i < numMatrixRows; ++i)
    {
      for (int j = 1; j < numMatrixColumns; ++j)
      {
        double minNumberTransformations, numberTransformationsBuffer;

        if (sourceWord.charAt(i - 1) == targetWord.charAt(j - 1))
        {
          minNumberTransformations = doNothingMethod.calculateCorrectionCost(
            dynamicMatrix,
            sourceWord, targetWord,
            i, j
          );
        }
        else
        {
          minNumberTransformations = additionMethod.calculateCorrectionCost(
            dynamicMatrix,
            sourceWord, targetWord,
            i, j
          );
          numberTransformationsBuffer = deletionMethod.calculateCorrectionCost(
            dynamicMatrix,
            sourceWord, targetWord,
            i, j
          );

          if (numberTransformationsBuffer < minNumberTransformations)
          {
            minNumberTransformations = numberTransformationsBuffer;
          }

          if (correctionMethods != null)
          {
            for (ICorrectionMethod correctionMethod : correctionMethods)
            {
              numberTransformationsBuffer = correctionMethod.calculateCorrectionCost(
                dynamicMatrix,
                sourceWord, targetWord,
                i, j
              );

              if (numberTransformationsBuffer < minNumberTransformations)
              {
                minNumberTransformations = numberTransformationsBuffer;
              }
            }
          }
        }

        dynamicMatrix[i][j] = minNumberTransformations;
      }
    }

    // the smallest number of transformations needed to transform sourceWord into targetWord is the last element of the
    // dynamicMatrix
    return dynamicMatrix[numMatrixRows - 1][numMatrixColumns - 1];
  }

  /**
   * <p>
   *   Based on a given set of knownWords chooses the closest to sourceWord from this set, utilizing edit distance
   *   calculated by {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)}.
   * </p>
   *
   * @param sourceWord A word for which closest will be calculated.
   * @param knownWords A Set of known correct words. A Set was chosen, since it guarantees uniqueness of elements.
   * @param correctionMethods Specifies which operations are available, whe calculation an edit distance.
   * @param addCorrectionMethodCost Cost of the AddCorrectionMethod.
   * @param deleteCorrectionMethodCost Cost of the DeleteCorrectionMethod.
   * @return Lexicographically SortedSet, which is a subset of knownWords, where all words have the same minimal edit
   * distance with a sourceWord. A SortedSet is chosen for it guarantees uniqueness and a lexicographical order of
   * elements in it.
   */
  public static SortedSet<String> getClosestWordsFromSet(
    String sourceWord,
    Set<String> knownWords,
    Iterable<ICorrectionMethod> correctionMethods,
    Optional<Integer> addCorrectionMethodCost,
    Optional<Integer> deleteCorrectionMethodCost
  )
  {
    SortedSet<String> closestWords = new TreeSet<>();
    double minimalEditDistance = Double.POSITIVE_INFINITY;

    for (String knownWord : knownWords)
    {
      double editDistanceToKnownWord =
        estimate(
          sourceWord,
          knownWord,
          correctionMethods,
          addCorrectionMethodCost,
          deleteCorrectionMethodCost
        );

      if (minimalEditDistance > editDistanceToKnownWord)
      {
        closestWords.clear();
        minimalEditDistance = editDistanceToKnownWord;
        closestWords.add(knownWord);
      }
      else if (DoubleComparator.areDoublesEqual(minimalEditDistance, editDistanceToKnownWord))
      {
        closestWords.add(knownWord);
      }

      if (sourceWord.equals(knownWord))
      {
        break;
      }
    }

    return closestWords;
  }

  /**Set
   * <p>
   *   Based on a given Map of known words (dictWithFrequencies) chooses the closest to sourceWord from this set,
   *   utilizing edit distance
   *   calculated by {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)} and frequencies of
   *   words in the given dictionary.
   * </p>
   *
   * @param sourceWord A word for which closest will be calculated
   * @param dictWithFrequencies A Map of known correct words along with their frequencies. A Map was chosen, since
   * it guarantees uniqueness of stored keys and allows to associate another value with keys, which are frequencies
   * in this case.
   * @param correctionMethods Specifies which operations are available, whe calculation an edit distance
   * @param addCorrectionMethodCost Cost of the AddCorrectionMethod.
   * @param deleteCorrectionMethodCost Cost of the DeleteCorrectionMethod.
   * @return Lexicographically SortedSet, which is a subset of knownWords, where all words have the same minimal edit
   * distance with a sourceWord and the same frequency in dictWithFrequencies. A SortedSet is chosen for it guarantees
   * uniqueness and a lexicographical order of elements in it.
   */
  public static SortedSet<String> getBestWordsFromDictWithFrequencies(
    String sourceWord,
    Map<String, Integer> dictWithFrequencies,
    Iterable<ICorrectionMethod> correctionMethods,
    Optional<Integer> addCorrectionMethodCost,
    Optional<Integer> deleteCorrectionMethodCost
  )
  {
    SortedSet<String> bestWords = new TreeSet<>();
    double minimalEditDistance = Double.POSITIVE_INFINITY;

    for (String knownWord : dictWithFrequencies.keySet())
    {
      double editDistanceToKnownWord =
        estimate(
          sourceWord,
          knownWord,
          correctionMethods,
          addCorrectionMethodCost,
          deleteCorrectionMethodCost
        );

      if (minimalEditDistance > editDistanceToKnownWord)
      {
        bestWords.clear();
        minimalEditDistance = editDistanceToKnownWord;
        bestWords.add(knownWord);
      }
      else if (DoubleComparator.areDoublesEqual(minimalEditDistance, editDistanceToKnownWord))
      {
        int freqComparisonResult =
          dictWithFrequencies.get(knownWord).compareTo(
            dictWithFrequencies.get(bestWords.first())
          );

        if (freqComparisonResult > 0)
        // frequency of a current knownWord is greater than a frequency of any best word found so far
        {
          bestWords.clear();
          bestWords.add(knownWord);
        }
        else if (freqComparisonResult == 0)
        // frequency of a current knownWord is a same as a frequency of any best word found so far
        {
          bestWords.add(knownWord);
        }
      }
    }

    return bestWords;
  }

  /**
   * ArrayList of operations to calculate Levenshtein distance, namely it contains only SubstitutionCorrectionMethod
   * with cost 1 (since addition and deletion are already a part of
   * {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)}) method).
   */
  public static final ArrayList<ICorrectionMethod> LevenshteinDistanceOperations;
  /**
   * ArrayList of operations to calculate Damerau–Levenshtein distance, namely it contains only
   * SubstitutionCorrectionMethod and TranspositionCorrectionMethod both with cost 1
   * (since addition and deletion are already a part of
   * {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)}) method).
   */
  public static final ArrayList<ICorrectionMethod> DamerauLevenshteinDistanceOperations;

  static
  {
    LevenshteinDistanceOperations = new ArrayList<>(1);
    LevenshteinDistanceOperations.add(new SubstitutionCorrectionMethod(1));

    DamerauLevenshteinDistanceOperations = new ArrayList<>(2);
    DamerauLevenshteinDistanceOperations.add(new SubstitutionCorrectionMethod(1));
    DamerauLevenshteinDistanceOperations.add(new TranspositionCorrectionMethod(1));
  }
}
