package edu.innopolis;

import java.util.Optional;

/**
 * <p>
 *   Represents an operation of substituting character sourceWord[srcMatrixI - 1] in string
 *   sourceWord[0..srcMatrixI - 1] to targetWord[trgMatrixI - 1] so that the sourceWord after all other transformations
 *   becomes targetWord[0..trgMatrixI - 1] in
 *   {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)}.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 */
public final class SubstitutionCorrectionMethod implements ICorrectionMethod
{
  /**
   * <p>
   *   Default constructor, initializes correctionCost with 1.0.
   * </p>
   */
  public SubstitutionCorrectionMethod() {}

  /**
   * @param initCorrectionCost A parameter, correctionCost is being initialized with
   * @throws IllegalArgumentException When initCorrectionCost < 1.0 the exception is thrown.
   */
  public SubstitutionCorrectionMethod(double initCorrectionCost) throws IllegalArgumentException
  {
    if (initCorrectionCost < 1.0)
    {
      throw new IllegalArgumentException("correctionCost of SubstitutionCorrectionMethod must be >= 1.0");
    }

    correctionCost = initCorrectionCost;
  }

  @Override
  public double calculateCorrectionCost(
    double dynamicMatrix[][],
    String sourceWord,
    String targetWord,
    int srcMatrixI, int trgMatrixI
  ) throws IndexOutOfBoundsException
  {
    return dynamicMatrix[srcMatrixI - 1][trgMatrixI - 1] + correctionCost;
  }

  private double correctionCost = 1;
}
