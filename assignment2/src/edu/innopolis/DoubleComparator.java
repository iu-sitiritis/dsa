package edu.innopolis;

/**
 * <p>
 *   Static class, that contains a method that tells whether 2 doubles are equal.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 */
public final class DoubleComparator
{
  private DoubleComparator() {}

  /**
   * <p>
   *   Function that tells whether 2 doubles are equal by subtracting one from another, taking absolute of the result
   *   and comparing it against a predefined static small threshold EPSILON. If the value is less than the threshold -
   *   left and right are close enough to each other, so they are considered equal.
   * </p>
   *
   * @param left First value to be compared
   * @param right Second value to be compared
   * @return true - doubles are equal, false otherwise
   */
  public static boolean areDoublesEqual(double left, double right)
  {
    return Math.abs(left - right) < EPSILON;
  }

  public static final double EPSILON = 0.00000001;
}
