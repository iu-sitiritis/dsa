package edu.innopolis;

import java.util.Optional;

/**
 * <p>
 *   Represents an operation of transposition a character sourceWord[srcMatrixI - 1] with sourceWord[srcMatrixI - 2]
 *   in string sourceWord[0..srcMatrixI - 1] so that the later after all other transformations
 *   becomes targetWord[0..trgMatrixI - 1] in
 *   {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)}.
 *   Transposition is only possible, when:
 *   srcMatrixI >= 2 && trgMatrixI >= 2 &&
 *   sourceWord[srcMatrixI - 1] == targetWord[trgMatrixI - 2] &&
 *   sourceWord[srcMatrixI - 2] == targetWord[trgMatrixI - 1].
 *   If transposition is not possible - the cost of the operation is Double.POSITIVE_INFINITY.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 */
public final class TranspositionCorrectionMethod implements ICorrectionMethod
{
  /**
   * <p>
   *   Default constructor, initializes correctionCost with 1.0.
   * </p>
   */
  public TranspositionCorrectionMethod() {}

  /**
   * @param initCorrectionCost A parameter, correctionCost is being initialized with
   * @throws IllegalArgumentException When initCorrectionCost < 1.0 the exception is thrown.
   */
  public TranspositionCorrectionMethod(double initCorrectionCost)
  {
    if (initCorrectionCost < 1.0)
    {
      throw new IllegalArgumentException("correctionCost of TranspositionCorrectionMethod must be >= 1.0");
    }

    correctionCost = initCorrectionCost;
  }

  /**
   * @param dynamicMatrix Array used by dynamic algorithm to calculate the cost of correcting a source word to transform
   * it into a target word. The matrix is of size [sourceWord.length + 1][targetWord.length + 1]. An array is chosen,
   * since the size of the matrix is known and not changed, once it is created and it guarantees O(1) access by index.
   * @param sourceWord Word that is to be transformed into the targetWord
   * @param targetWord Word, sourceWord is transformed into
   * @param srcMatrixI Current row index of the dynamicMatrix
   * @param trgMatrixI Current column index of the dynamicMatrix
   * @exception IndexOutOfBoundsException If srcI or trgI is invalid indexes the exception is thrown.
   * @return Number of corrections needed to transform sourceWord[0..srcMatrixI - 1] into targetWord[0..targetWord - 1],
   * based on dynamicMatrix calculated so far, srcI, trgI and the correction method's cost. If a transposition is not
   * possible Double.POSITIVE_INFINITY is returned.
   */
  @Override
  public double calculateCorrectionCost(
    double dynamicMatrix[][],
    String sourceWord,
    String targetWord,
    int srcMatrixI, int trgMatrixI
  ) throws IndexOutOfBoundsException
  {
    double result = Double.POSITIVE_INFINITY;

    if (
      (srcMatrixI >= 2) && (trgMatrixI >= 2) &&
      (sourceWord.charAt(srcMatrixI - 1) == targetWord.charAt(trgMatrixI - 2)) &&
      (sourceWord.charAt(srcMatrixI - 2) == targetWord.charAt(trgMatrixI - 1))
    )
    // 2 chars can be swapped
    {
      result = dynamicMatrix[srcMatrixI - 2][trgMatrixI - 2] + correctionCost;
    }

    return result;
  }

  private double correctionCost = 1;
}
