package edu.innopolis;

import java.io.IOException;
import java.util.*;

/**
 * <p>
 *   Contains functions that correspond to paragraphs from coding part of
 *   <a href="https://moodle.innopolis.university/mod/assign/view.php?id=5926">
 *     the assignment
 *   </a>.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 */
public class Main
{

  /**
   * <p>
   *   Set of operations available while calculating Damerau-Levenshtein edit distance, since it is the distance defined
   *   by the assignment.
   * </p>
   */
  private static final Iterable<ICorrectionMethod> correctionMethods =
    StringCorrector.DamerauLevenshteinDistanceOperations;

  /**
   * <p>Codding part. Paragraph 2.1.</p>
   */
  static void misspellings()
  {
    Scanner inputScanner = new Scanner(System.in);

    int numWords = inputScanner.nextInt();
    // Using ArrayList, since int is already known how many numbers there will be in output, so the memory is allocated
    // once for the result and the get access to an ArrayList is O(1)
    ArrayList<Double> editDistances = new ArrayList<>(numWords);

    for (int i = 0; i < numWords; ++i)
    {
      String sourceWord = inputScanner.next();
      String targetWord = inputScanner.next();
      editDistances.add(
        StringCorrector.estimate(
          sourceWord,
          targetWord,
          correctionMethods,
          Optional.of(1),
          Optional.of(1)
        )
      );
    }

    for (int i = 0; i < numWords; i++)
    {
      System.out.println(editDistances.get(i).intValue());
    }
  }

  /**
   * <p>Codding part. Paragraph 2.2.</p>
   */
  static void correctionSuggestions()
  {
    Scanner inputScanner = new Scanner(System.in);

    int numKnownWords = inputScanner.nextInt();
    // TreeSet is chosen, since the getClosestWordsFromSet is interested in a set of unique words, which is guaranteed
    // by the data structure. In addition, no hash collisions will occur, since the DS does not use any hash function
    // and it guarantees O(log(n)) asymptotic cost for basic operations (add, remove and contains).
    Set<String> knownWords = new TreeSet<>();

    for (int i = 0; i < numKnownWords; ++i)
    {
      knownWords.add(inputScanner.next());
    }

    String sourceWord = inputScanner.next();
    SortedSet<String> closestWords = StringCorrector.getClosestWordsFromSet(
      sourceWord,
      knownWords,
      correctionMethods,
      Optional.of(1),
      Optional.of(1)
    );

    // since the closestWords is a sorted collection of words, the iterator returned  is ascending and guarantees
    // that the closest words will be printed in an ascending lexicographical order
    Iterator<String> iterator = closestWords.iterator();
    while (iterator.hasNext())
    {
      System.out.print(iterator.next());

      if (iterator.hasNext())
      {
        System.out.print(" ");
      }
    }
  }

  /**
   * <p>Codding part. Paragraph 2.3.</p>
   */
  static void textAutocorrection() throws IOException
  {
    // read correct words into a dictionary and calculate their frequency

    /*
      TreeMap is chosen here for the sae reason as was TreeSet in correctionSuggestions, however in this task
      in addition to the word stored also its frequency myst be stored.
    */
    Map<String, Integer> dictWithFrequencies = new TreeMap<>();
    /*
      StringBuilder is chosen over a regular String, since the last is immutable, whereas the prior is created for the
      purpose of dynamically building a string of characters.
    */
    StringBuilder readSequence = new StringBuilder();
    char currentChar;
    boolean readingWord = false;
    while ((currentChar = (char) System.in.read()) != '\n')
    {
      if (Character.isLetter(currentChar))
      {
        if (!readingWord)
        {
          // starting reading a word
          readingWord = true;
        }

        readSequence.append(currentChar);
      }
      else
      {
        if (readingWord)
        {
          // a word is read no more
          readingWord = false;

          // add the read word to the dictionary or update its frequency
          String readWord = readSequence.toString();
          if (dictWithFrequencies.containsKey(readWord))
          {
            dictWithFrequencies.replace(readWord, dictWithFrequencies.get(readWord) + 1);
          }
          else
          {
            dictWithFrequencies.put(readWord, 1);
          }

          // clear read sequence
          readSequence.delete(0, readSequence.length());
        }
      }
    }

    if (readingWord)
    // if the end of the first line was reached, though the last word was added to the dictionary - add it
    {
      // add the read word to the dictionary or update its frequency
      String readWord = readSequence.toString();
      if (dictWithFrequencies.containsKey(readWord))
      {
        dictWithFrequencies.replace(readWord, dictWithFrequencies.get(readWord) + 1);
      }
      else
      {
        dictWithFrequencies.put(readWord, 1);
      }
    }

    // while reading the text for autocorrection - perform the autocorrection
    readSequence.delete(0, readSequence.length());
    readingWord = false;
    while (
      (System.in.available() > 0) &&
      (
        (currentChar = (char) System.in.read()) != '\n'
      )
    )
    // read chars till the end
    {
      if (Character.isLetter(currentChar))
      {
        if (!readingWord)
        {
          // starting reading a word
          readingWord = true;
        }

        readSequence.append(currentChar);
      }
      else
      {
        if (readingWord)
        {
          // a word is read no more
          readingWord = false;

          // find the best substitution for the read word and print it
          System.out.print(
            StringCorrector.getBestWordsFromDictWithFrequencies(
              readSequence.toString(),
              dictWithFrequencies,
              correctionMethods,
              Optional.of(1),
              Optional.of(1)
            ).first()
          );

          // clear read sequence
          readSequence.delete(0, readSequence.length());
        }

        System.out.print(currentChar);
      }
    }

    if (readingWord)
    // if the end of the input stream was reached, though the last word was not printed - print it
    {
      // find the best substitution for the read word and print it
      System.out.print(
        StringCorrector.getBestWordsFromDictWithFrequencies(
          readSequence.toString(),
          dictWithFrequencies,
          correctionMethods,
          Optional.of(1),
          Optional.of(1)
        ).first()
      );

      System.out.print(currentChar);
    }
  }

  public static void main(String[] args) throws IOException
  {
    textAutocorrection();
  }
}
