package edu.innopolis;


import java.util.Optional;

/**
 * <p>
 *   Represents an operation of doing nothing, while performing a 2D dynamic in
 *   {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)}.
 *   It is used when the 2 current characters of 2 strings are the same.
 *   The correction cost of this operation is always 0.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 */
public class DoNothingCorrectionMethod implements ICorrectionMethod
{
  /**
   * <p>
   *   Creates a new instance of DoNothingCorrectionMethod with correctionCost == 0.0.
   * </p>
   */
  public DoNothingCorrectionMethod() {}

  @Override
  public double calculateCorrectionCost(
    double[][] dynamicMatrix,
    String sourceWord, String targetWord,
    int srcMatrixI, int trgMatrixI
  ) throws IndexOutOfBoundsException
  {
    return dynamicMatrix[srcMatrixI - 1][trgMatrixI - 1];
  }

  double correctionCost = 0.0;
}
