package edu.innopolis;

import java.util.Optional;

/**
 * <p>
 *   Represents an operation of deleting character sourceWord[srcMatrixI - 1] from string sourceWord[0..srcMatrixI - 1]
 *   so that the later after all other transformations becomes targetWord[0..trgMatrixI - 1] in
 *   {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)}.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 */
public final class DeleteCorrectionMethod implements ICorrectionMethod
{
  /**
   * <p>
   *   Default constructor, initializes correctionCost with 1.0.
   * </p>
   */
  public DeleteCorrectionMethod() {}

  /**
   * @param initCorrectionCost A parameter, correctionCost is being initialized with
   * @throws IllegalArgumentException When initCorrectionCost < 1.0 the exception is thrown.
   */
  public DeleteCorrectionMethod(double initCorrectionCost) throws IllegalArgumentException
  {
    if (initCorrectionCost < 1.0)
    {
      throw new IllegalArgumentException("correctionCost of DeleteCorrectionMethod must be >= 1.0");
    }

    correctionCost = initCorrectionCost;
  }

  @Override
  public double calculateCorrectionCost(
    double dynamicMatrix[][],
    String sourceWord,
    String targetWord,
    int srcMatrixI, int trgMatrixI
  ) throws IndexOutOfBoundsException
  {
    return dynamicMatrix[srcMatrixI - 1][trgMatrixI] + correctionCost;
  }

  private double correctionCost = 1;
}
