package edu.innopolis;

import java.util.Optional;

/**
 * <p>
 *   Represents an operation of adding character sourceWord[srcMatrixI - 1] to string sourceWord[0..srcMatrixI - 2]
 *   so that the later becomes targetWord[0..trgMatrixI - 1] in
 *   {@link StringCorrector#estimate(String, String, Iterable, Optional, Optional)}.
 * </p>
 *
 * @author Tymur Lysenko (Telegram: @sitiritis)
 */
public final class AddCorrectionMethod implements ICorrectionMethod
{
  /**
   * <p>
   *   Default constructor, initializes correctionCost with 1.0.
   * </p>
   */
  public AddCorrectionMethod() {};

  /**
   * @param initCorrectionCost A parameter, correctionCost is being initialized with
   * @throws IllegalArgumentException When initCorrectionCost < 1.0 the exception is thrown.
   */
  public AddCorrectionMethod(double initCorrectionCost) throws IllegalArgumentException
  {
    if (initCorrectionCost < 1.0)
    {
      throw new IllegalArgumentException("correctionCost of AddCorrectionMethod must be >= 1.0");
    }

    correctionCost = initCorrectionCost;
  }

  @Override
  public double calculateCorrectionCost(
    double dynamicMatrix[][],
    String sourceWord,
    String targetWord,
    int srcMatrixI, int trgMatrixI
  ) throws IndexOutOfBoundsException
  {
    return dynamicMatrix[srcMatrixI][trgMatrixI - 1] + correctionCost;
  }

  private double correctionCost = 1.0;
}
