package edu.innopolis;

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Heap abstract data type.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
public interface IHeap<K extends Comparable<K>, V>
{
  /**
   * <p>
   *  Represents an entry in a heap. Needs not necessarily to be stored by an implementation. Used to be returned by
   *  methods defined in this ATD.
   * </p>
   * @param <K> Type of a stored key.
   * @param <V> Type of a stored value.
   */
  class HeapEntry<K extends Comparable, V>
  {
    /**
     * <p>
     *   Constructs a new heap entry with a given key and an associated value.
     * </p>
     * @param keyInit Value of a key stored in the object.
     * @param valueInit Value of a value stored in the object.
     */
    HeapEntry(K keyInit, V valueInit)
    {
      key = keyInit;
      value = valueInit;
    }

    public K getKey()
    {
      return key;
    }

    public V getValue()
    {
      return value;
    }

    /**
     * <p>
     *   Key stored in a heap. Uniqueness is defined by implementation.
     * </p>
     */
    private K key;
    /**
     * <p>
     *   Value associated to the key.
     * </p>
     */
    private V value;
  }

  /**
   * <p>
   *   Insert a new entry into a heap.
   * </p>
   * @param key Key to insert.
   * @param value Associated value to insert.
   * @return Returns this to enable chain calls.
   */
  IHeap<K, V> insert(K key, V value);

  /**
   * @return HeapEntry<K, V> stored on top of the heap without removing it.
   */
  HeapEntry<K, V> top();

  /**
   * @return HeapEntry<K, V> stored on top of the heap and removing it.
   */
  HeapEntry<K, V> removeTop();

  /**
   * <p>
   *   Removes all elements from heap without returning anything.
   * </p>
   */
  void clear();

  /**
   * @return Size of the current heap.
   */
  int size();

  /**
   * @return true if heap is empty, false otherwise.
   */
  default boolean isEmpty()
  {
    return size() <= 0;
  }
}
