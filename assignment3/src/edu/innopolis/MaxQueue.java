package edu.innopolis;

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Maximal priority queue.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
public class MaxQueue<K extends Comparable<K>, V> extends BinomialHeap<K, V>
{
  /**
   * <p>
   *   Constructs an empty priority queue.
   * </p>
   */
  public MaxQueue()
  {
    super(Priority.MAX);
  }

  /**
   * <p>
   *   Tries to deep copy other priority queue. If a copy constructor is not supported by K or V, references are copied
   *   instead.
   * </p>
   * @param other Priority queue to copy from.
   */
  public MaxQueue(MaxQueue<K, V> other)
  {
    super((BinomialHeap<K, V>)other);
  }

  /**
   * @return Returns maximal key and associated value stored in the queue without removing it.
   */
  public HeapEntry<K, V> max()
  {
    return top();
  }

  /**
   * @return Returns maximal key and associated value stored in the queue and removes it.
   */
  public HeapEntry<K, V> removeMax()
  {
    return removeTop();
  }
}
