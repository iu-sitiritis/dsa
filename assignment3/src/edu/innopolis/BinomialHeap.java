package edu.innopolis;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Represents a binomial heap.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
public class BinomialHeap<K extends Comparable<K>, V> implements IMergeableHeap<BinomialHeap<K, V>, K, V>
{
  /**
   * <p>
   *   Constructs an empty binomial heap with a given priority.
   * </p>
   * @param priorityInit Defines a behavior of the tree. If MIN - then a this thee is a minimal binomial tree, storing
   *                     the smallest element on the top. If MAX - then a this thee is a maximal binomial tree, storing
   *                     the largest element on the top.
   */
  public BinomialHeap(Priority priorityInit)
  {
    priority = priorityInit;
    trees = new LinkedList<>();
    size = 0;
  }

  /**
   * <p>
   *   Tries to deep copy other binomial heap. If a copy constructor is not supported by K or V, references are copied
   *   instead.
   * </p>
   * @param other Binomial heap to copy from.
   */
  public BinomialHeap(BinomialHeap<K, V> other)
  {
    size = other.size;
    priority = other.priority;

    trees = new LinkedList<>();
    for (BinomialTree<K, V> tree : other.trees)
    {
      trees.addLast(new BinomialTree<>(tree));
    }
  }

  /**
   * <p>
   *   Constructs a new binomial heap from a given list of binomial trees and a priority. Does not copy given trees into
   *   a new instance.
   * </p>
   * @param treesInit Trees to construct a binomial heap from.
   * @param priorityInit Defines a behavior of the tree. If MIN - then a this thee is a minimal binomial tree, storing
   *                     the smallest element on the top. If MAX - then a this thee is a maximal binomial tree, storing
   *                     the largest element on the top.
   */
  private BinomialHeap(LinkedList<BinomialTree<K, V>> treesInit, Priority priorityInit)
  {
    trees = treesInit;
    priority = priorityInit;
  }

  /**
   * <p>
   *   Merges all elements from the second binomial heap into this binomial heap, removing it form the original. If
   *   priorities are different or other is null - an exception is thrown.
   * </p>
   * <p>
   *   Since the algorithm iterates over all trees in the forests of both this and the other heap, which have maximal
   *   sizes 1 + log(size) & 1 + log(other.size) respectively, applying constant operations for each element and then
   *   calls a balance() method, running time of which will be O(2 + log(size) + log(other.size)) it's running time is:
   *   O(log(size) + log(other.size))
   * </p>
   * @param other Heap to take elements from.
   * @return Returns this to enable chain calls.
   */
  public BinomialHeap<K, V> merge(BinomialHeap<K, V> other)
  {
    if (other == null)
    {
      throw new NullPointerException("Binomial heap to link is null");
    }
    if (priority != other.priority)
    {
      throw new IllegalArgumentException("Minimal and maximal binomial heaps can not be merged");
    }
    else
    {
      LinkedList<BinomialTree<K, V>> newTrees = new LinkedList<>();
      LinkedList<BinomialTree<K, V>> biggerForest;
      LinkedList<BinomialTree<K, V>> smallerForest;

      if (trees.size() > other.trees.size())
      {
        biggerForest = trees;
        smallerForest = other.trees;
      }
      else
      {
        biggerForest = other.trees;
        smallerForest = trees;
      }

      while (!smallerForest.isEmpty())
      {
        BinomialTree<K, V> curTreeFirst = smallerForest.removeFirst();
        BinomialTree<K, V> curTreeSecond = biggerForest.removeFirst();

        if (curTreeFirst.getOrder() > curTreeSecond.getOrder())
        {
          newTrees.addLast(curTreeSecond);
          newTrees.addLast(curTreeFirst);
        }
        else
        {
          newTrees.addLast(curTreeFirst);
          newTrees.addLast(curTreeSecond);
        }
      }

      while (!biggerForest.isEmpty())
      {
        newTrees.addLast(biggerForest.removeFirst());
      }

      trees = newTrees;
      balance();
      size += other.size;
      other.size = 0;
      return this;
    }
  }

  /**
   * <p>
   *   Running time - O(log(size)), since that is the time taken by a balance() method, other statements are constants.
   * </p>
   * @param key Key to insert.
   * @param value Associated value to insert.
   * @return Returns this to enable chain calls.
   */
  @Override
  public BinomialHeap<K, V> insert(K key, V value)
  {
    trees.addFirst(new BinomialTree<>(key, value, priority));
    balance();
    ++size;

    return this;
  }

  /**
   * <p>
   *   Running time - O(log(size)), since number of trees in the heap is 1 + log2(size).
   * </p>
   * @return Returns a binomial tree, root of which is maximal or minimal, depending on a priority. Since this class is
   * a binomial heap the root will contain a global extremum among all stored elements.
   */
  private BinomialTree<K, V> topTree()
  {
    Iterator<BinomialTree<K, V>> treeRootIterator = trees.iterator();
    BinomialTree<K, V> top = (treeRootIterator.hasNext()) ? treeRootIterator.next() : null;

    switch (priority)
    {
      case MAX:
      {
        while (treeRootIterator.hasNext())
        {
          BinomialTree<K, V> curTree = treeRootIterator.next();

          if (curTree.compareTo(top) > 0)
          {
            top = curTree;
          }
        }

        break;
      }
      case MIN:
      {
        while (treeRootIterator.hasNext())
        {
          BinomialTree<K, V> curTree = treeRootIterator.next();

          if (curTree.compareTo(top) < 0)
          {
            top = curTree;
          }
        }

        break;
      }
      default:
      {
        throw new IllegalStateException("Illegal state. Binomial heap can only be a minimal or maximal.");
      }
    }

    return top;
  }

  /**
   * <p>
   *   Running time - O(log(size)), since number of trees in the heap is 1 + log2(size).
   * </p>
   */
  @Override
  public HeapEntry<K, V> top()
  {
    HeapEntry<K, V> topEntry = null;

    if (!isEmpty())
    {
      topEntry = topTree().getRoot().toHeapEntry();
    }

    return topEntry;
  }

  /**
   * <p>
   *   Returns minimal or maximal key-value pair, depending on priority and removes it from the heap. If heap is empty
   *   an exception is thrown.
   * </p>
   * <p>
   *   Since topTree() takes O(log(size)) time, and link takes O(max(log(size), log(other.size))), where other is a
   *   forest of all children of a tree, root node of which is extremum, therefore other.size < size, hence running time
   *   is:
   *   O(log(size))
   * </p>
   */
  @Override
  public HeapEntry<K, V> removeTop()
  {
    if (isEmpty())
    {
      throw new IllegalStateException("Binomial heap is empty");
    }
    else
    {
      BinomialTree<K, V> curTopTree = topTree();
      trees.remove(curTopTree);
      --size;

      merge(new BinomialHeap<>(curTopTree.childrenForest(), priority));

      return curTopTree.getRoot().toHeapEntry();
    }
  }

  @Override
  public void clear()
  {
    trees.clear();
    size = 0;
  }


  /**
   * <p>
   *   At certain point in time it might be the case, when there are multiple binomial trees of the same order, then
   *   they must be merged together until there are no such trees in the forest. It iterates from left to right through
   *   all binomial trees and tries to link them, if it is possible, until no trees with different orders are left in
   *   the forest.
   * </p>
   * <p>
   *   Since the method iterates binomial forest once and for binomial heaps consisting of size nodes, maximal number of
   *   binomial trees is 1 + log2(size), running time is:
   *   O(log(size)).
   * </p>
   */
  private void balance()
  {
    if (!trees.isEmpty())
    {
      LinkedList<BinomialTree<K, V>> newTrees = new LinkedList<>();
      Iterator<BinomialTree<K, V>> treesIterator = trees.iterator();
      boolean proceedBalance = treesIterator.hasNext();
      BinomialTree<K, V> firstTree = null;

      if (proceedBalance)
      {
        firstTree = treesIterator.next();
      }

      while (proceedBalance)
      {
        if (treesIterator.hasNext())
        {
          BinomialTree<K, V> curTree = treesIterator.next();

          if (firstTree.canLink(curTree))
          {
            firstTree.link(curTree);
          }
          else if (firstTree.getOrder() > curTree.getOrder())
          {
            newTrees.addLast(curTree);
          }
          else
          {
            newTrees.addLast(firstTree);
            firstTree = curTree;
          }
        }
        else
        {
          newTrees.addLast(firstTree);
          proceedBalance = false;
        }

        // loop invariant
        if (newTrees.size() > 1)
        {
          assert
            newTrees.getLast().getOrder() > newTrees.get(newTrees.size() - 2).getOrder()
            : "Balance loop invariant is violated!";
        }
      }

      trees = newTrees;
    }
  }


  /**
   * @return Number of elements stored in a heap.
   */
  @Override
  public int size()
  {
    return size;
  }

  /**
   * @return Priority of this binomial tree.
   */
  public Priority getPriority()
  {
    return priority;
  }

  /**
   * <p>
   *   Forest of binomial trees sorted by tree order. The sort order is managed by this class.
   * </p>
   * <p>
   *   For binomial heaps consisting of size nodes, number of binomial trees is at most
   *   <a href="https://en.wikipedia.org/wiki/Binomial_heap">1 + log2(size)</a>.
   * </p>
   */
  private LinkedList<BinomialTree<K, V>> trees;
  /**
   * <p>
   *   Priority of this binomial heap. It defines the behavior of the heap. If MIN - then a this heap is a minimal
   *   binomial heap, i. e. the top element is the smallest. If MAX - then a this heap is a maximal binomial heap,
   *   i. e. the top element is the largest.
   * </p>
   */
  private Priority priority;
  /**
   * Number of elements stored in the binomial heap.
   */
  private int size;
}
