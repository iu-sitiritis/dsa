package edu.innopolis;

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Represents a meargeable heap ADT, that is a heap that can be merged with another heap.
 * </p>
 * @param <T> Type of an other heap that an implementer is able to take elements from.
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
public interface IMergeableHeap<T extends IMergeableHeap<?, K, V>, K extends Comparable<K>, V> extends IHeap<K, V>
{
  /**
   * <p>
   *   Takes all elements from a given other heap and merges them into the current one, removing it from the other.
   * </p>
   * @param other Heap to take elements from.
   * @return Returns this to enable chain calls.
   */
  IMergeableHeap<T, K, V> merge(T other);
}
