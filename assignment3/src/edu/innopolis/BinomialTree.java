package edu.innopolis;

import java.util.LinkedList;

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Represents a binomial tree.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
public class BinomialTree<K extends Comparable<K>, V> implements Comparable<BinomialTree<K, V>>
{
  /**
   * <p>
   *   Constructs a binomial tree with 1 node , storing a given key and value.
   * </p>
   * @param key Key to be stored in a node.
   * @param value Value to be stored in a node.
   * @param priorityInit Defines a behavior of the tree. If MIN - then a this tree is a minimal binomial tree, storing
   *                     the smallest element on the top. If MAX - then a this tree is a maximal binomial tree, storing
   *                     the largest element on the top.
   */
  public BinomialTree(K key, V value, Priority priorityInit)
  {
    root = new BinomialTreeNode<>(key, value);
    priority = priorityInit;
  }

  /**
   * <p>
   *   Copy constructor. Constructs a new binomial tree recursively copying all nodes from an other given binomial tree
   *   with their key and values utilizing BinomialTreeNode copy constructor. Also, copies other's priority.
   * </p>
   * @param other Other binomial tree to copy from.
   */
  public BinomialTree(BinomialTree<K, V> other)
  {
    priority = other.priority;
    root = new BinomialTreeNode<>(other.root);
  }

  /**
   * <p>
   *   Constructs a binomial tree from a given root with a given priority, assigning  It does not copy nodes from a
   *   given root node.
   * </p>
   * @param rootInit Root node to construct a tree from.
   * @param priorityInit Defines a behavior of the tree. If MIN - then a this thee is a minimal binomial tree, storing
   *                     the smallest element on the top. If MAX - then a this thee is a maximal binomial tree, storing
   *                     the largest element on the top.
   */
  private BinomialTree(BinomialTreeNode<K, V> rootInit, Priority priorityInit)
  {
    root = rootInit;
    priority = priorityInit;
  }

  /**
   * <p>
   *   Defines if this binomial tree can be merged with an other given binomial tree. Binomial trees can be merged if
   *   they are of the same size.
   * </p>
   * @param other Binomial tree to test if it can be used to link values from it into this tree.
   * @return true - trees can be merged; false otherwise.
   */
  public boolean canLink(BinomialTree<K, V> other)
  {
    return (other != null) && (getOrder() == other.getOrder());
  }

  /**
   * <p>
   *   Merges other binomial tree into the current, if this operation is legal, otherwise IllegalArgumentException is
   *   thrown. Other tree remains untouched.
   * </p>
   * <p>
   *   Running time - O(1).
   * </p>
   * @param other Tree to link into this tree.
   * @return Returns this to enable chain calls.
   */
  public BinomialTree<K, V> link(final BinomialTree<K, V> other)
  {
    if (canLink(other))
    {
      BinomialTreeNode<K, V> newRoot;
      BinomialTreeNode<K, V> rootToMerge;

      switch (priority)
      {
        case MAX:
        {
          if (root.compareTo(other.root) > 0)
          {
            newRoot = root;
            rootToMerge = other.root;
          }
          else
          {
            newRoot = other.root;
            rootToMerge = root;
          }

          break;
        }
        case MIN:
        {
          if (root.compareTo(other.root) < 0)
          {
            newRoot = root;
            rootToMerge = other.root;
          }
          else
          {
            newRoot = other.root;
            rootToMerge = root;
          }

          break;
        }
        default:
        {
          throw new IllegalStateException("Illegal state. Binomial tree can only be a minimal or maximal.");
        }
      }

      root = newRoot.addChildrenLast(rootToMerge);
    }
    else
    {
      throw new IllegalArgumentException("Cannot link binomial trees of different orders.");
    }

    return this;
  }

  /**
   * @return Order of this binomial tree.
   */
  public int getOrder()
  {
    return root.getChildren().size();
  }

  /**
   * @return Root of this binomial tree.
   */
  public BinomialTreeNode<K, V> getRoot()
  {
    return root;
  }

  /**
   * @return Priority of this binomial tree.
   */
  public Priority getPriority()
  {
    return priority;
  }

  /**
   * <p>
   *   Compares 2 binomial trees by their roots utilizing BinomialTreeNode.compareTo().
   * </p>
   * @param other Other tree to compare to this.
   * @return negative value - this < other; 0 - this == other; positive value - this > other.
   */
  @Override
  public int compareTo(BinomialTree<K, V> other)
  {
    return root.compareTo(other.root);
  }

  /**
   * @return List of children of a root node as a forest of binomial trees, root of each is a child node of this
   * binomial tree's root.
   */
  public LinkedList<BinomialTree<K, V>> childrenForest()
  {
    LinkedList<BinomialTree<K, V>> treesOfChildren = new LinkedList<>();

    for (BinomialTreeNode<K, V> rootOfTree : root.getChildren())
    {
      treesOfChildren.addLast(new BinomialTree<>(rootOfTree, priority));
    }

    return treesOfChildren;
  }

  /**
   * <p>
   *   Root of this binomial tree.
   * </p>
   */
  private BinomialTreeNode<K, V> root;
  /**
   * <p>
   *   Priority of this binomial tree. It defines the behavior of the tree. If MIN - then a this tree is a minimal
   *   binomial tree, storing the smallest element on the top. If MAX - then a this tree is a maximal binomial tree,
   *   storing the largest element on the top.
   * </p>
   */
  private Priority priority;
}
