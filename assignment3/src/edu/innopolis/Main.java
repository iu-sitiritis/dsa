package edu.innopolis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tymur Lysenko. @sitiritis
 */
public class Main
{
  public static void simpleMaxQueueTest()
  {
    MaxQueue<Integer, String> q1 = new MaxQueue<>();
    MaxQueue<Integer, String> q2 = new MaxQueue<>();

    q1.insert(5, "Innopolis");
    q1.insert(4, "University");

    q2.insert(7, "It");
    q2.insert(5, "Works");
    q2.insert(11, "How?");

    q1.insert(2, "World");
    q1.insert(4, "Hello");

    q1.merge(q2);

    while (!q1.isEmpty())
    {
      IHeap.HeapEntry<Integer, String> curElement = q1.removeTop();
      System.out.println(curElement.getKey() + " " + curElement.getValue());
    }
  }

  public static void trickyMaxQueueTestCase()
  {
    MaxQueue<Integer, Integer> q1 = new MaxQueue<>();
    MaxQueue<Integer, Integer> q2 = new MaxQueue<>();

    q1.insert(50, 50);
    q1.insert(100, 100);
    q1.insert(75, 75);
    q1.insert(20, 20);
    q1.insert(17, 17);

    q2.insert(59, 59);
    q2.insert(72, 72);
    q2.insert(200, 200);
    q2.insert(38, 38);
    q2.insert(42, 42);
    q2.insert(0, 0);
    q2.insert(7, 7);

    q1.merge(q2);

    while (!q1.isEmpty())
    {
      IHeap.HeapEntry<Integer, Integer> curEntry = q1.removeMax();
      System.out.println(curEntry.getKey() + " " + curEntry.getValue());
    }
  }

  public static void cormenTestCase()
  {
    MinQueue<Integer, Integer> q1 = new MinQueue<>();
    MinQueue<Integer, Integer> q2 = new MinQueue<>();

    q1.insert(41, 41);
    q1.insert(28, 28);
    q1.insert(33, 33);
    q1.insert(15, 15);
    q1.insert(7, 7);
    q1.insert(25, 25);
    q1.insert(12, 12);

    q2.insert(55, 55);
    q2.insert(45, 45);
    q2.insert(32, 32);
    q2.insert(30, 30);
    q2.insert(24, 24);
    q2.insert(23, 23);
    q2.insert(22, 22);
    q2.insert(8, 8);
    q2.insert(50, 50);
    q2.insert(48, 48);
    q2.insert(29, 29);
    q2.insert(31, 31);
    q2.insert(17, 17);
    q2.insert(10, 10);
    q2.insert(44, 44);
    q2.insert(6, 6);
    q2.insert(27, 27);
    q2.insert(3, 3);
    q2.insert(37, 37);
    q2.insert(18, 18);

    q1.merge(q2);

    while (!q1.isEmpty())
    {
      IHeap.HeapEntry<Integer, Integer> curEntry = q1.removeMin();
      System.out.println(curEntry.getKey() + " " + curEntry.getValue());
    }
  }

  public static void todoList() throws ParseException
  {
    SortedMap<Date, MaxQueue<Integer, String>> todoList = new TreeMap<>();

    Pattern addCommandPat = Pattern.compile("^(\\d{2}\\.\\d{2}\\.\\d{2}) (.+?) (\\d+)");
    Pattern doCommandPat = Pattern.compile("^DO (\\d{2}\\.\\d{2}\\.\\d{2})");
    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
    Scanner input = new Scanner(System.in);

    int numCommands = Integer.parseInt(input.nextLine());

    // input & logic
    for (int i = 0; i < numCommands; ++i)
    {
      String curLine = input.nextLine();
      Matcher addMatcher = addCommandPat.matcher(curLine);

      if (addMatcher.find())
      {
        Date taskDate = dateFormat.parse(addMatcher.group(1));

        if (todoList.containsKey(taskDate))
        {
          todoList.get(taskDate).insert(
            Integer.parseInt(addMatcher.group(3)), // priority
            addMatcher.group(2) // task name
          );
        }
        else
        {
          MaxQueue<Integer, String> newQ = new MaxQueue<>();
          newQ.insert(
            Integer.parseInt(addMatcher.group(3)), // priority
            addMatcher.group(2) // task name
          );

          todoList.put(taskDate, newQ);
        }
      }
      else
      {
        Matcher doMatcher = doCommandPat.matcher(curLine);

        if (doMatcher.find())
        {
          Date taskDate = dateFormat.parse(doMatcher.group(1));

          if (todoList.containsKey(taskDate))
          {
            MaxQueue<Integer, String> dateToDoList = todoList.get(taskDate);

            if (!dateToDoList.isEmpty())
            {
              dateToDoList.removeMax();
            }
          }
        }
        else
        {
          System.out.println("Invalid input");
          break;
        }
      }
    }

    input.close();

    // copy all todo lists into one
    MaxQueue<Integer, String> totalTodoList = new MaxQueue<>();

    for (Map.Entry<Date, MaxQueue<Integer, String>> dateTodoListEntry : todoList.entrySet())
    {
      totalTodoList.merge(new MaxQueue<>(dateTodoListEntry.getValue()));
    }

    // output
    for (Map.Entry<Date, MaxQueue<Integer, String>> dateTodoListEntry : todoList.entrySet())
    {
      MaxQueue<Integer, String> dateTodoList = dateTodoListEntry.getValue();

      System.out.println("TODOList " + dateFormat.format(dateTodoListEntry.getKey()));

      while (!dateTodoList.isEmpty())
      {
        System.out.println("  " + dateTodoList.removeMax().getValue());
      }
    }

    System.out.println("TODOList");
    while (!totalTodoList.isEmpty())
    {
      System.out.println("  " + totalTodoList.removeMax().getValue());
    }
  }

  public static void main(String[] args) throws ParseException
  {
//    simpleMaxQueueTest();
//    trickyMaxQueueTestCase();
//    cormenTestCase();
    todoList();
  }
}
