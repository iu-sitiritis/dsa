package edu.innopolis;

import java.lang.reflect.Constructor;
import java.util.LinkedList;

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Represents a node in a binomial tree.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
public class BinomialTreeNode<K extends Comparable<K>, V> implements Comparable<BinomialTreeNode<K, V>>
{
  /**
   * <p>
   *   Constructs a new BinomialTreeNode<K, V> storing a given key and value and an empty list of child nodes.
   * </p>
   * @param keyInit Value of a key stored in the object.
   * @param valueInit Value of a value stored in the object.
   */
  public BinomialTreeNode(K keyInit, V valueInit)
  {
    key = keyInit;
    value = valueInit;
    children = new LinkedList<>();
  }

  /**
   * <p>
   *   Copy constructor. Constructs a new BinomialTreeNode<K, V> trying to recursively deep copy keys, values and
   *   children from an other given node using a copy constructor. If no copy constructor exists for K or V, just
   *   assigns references from the other.
   * </p>
   * @param other Node to copy from.
   */
  public BinomialTreeNode(BinomialTreeNode<K, V> other)
  {
    // try to deep copy key & value via reflection
    Class<?> keyClass = other.key.getClass();

    try
    {
      Constructor<?> keyCopyConstructor = keyClass.getConstructor(keyClass);
      key = (K) keyCopyConstructor.newInstance(other.key);
    }
    catch (Exception ex)
    {
      key = other.key;
    }

    Class<?> valueClass = other.value.getClass();

    try
    {
      Constructor<?> valueCopyConstructor = valueClass.getConstructor(valueClass);
      value = (V) valueCopyConstructor.newInstance(other.value);
    }
    catch (Exception ex)
    {
      value = other.value;
    }

    children = new LinkedList<>();
    for (BinomialTreeNode<K, V> treeNode : other.children)
    {
      children.addLast(new BinomialTreeNode<>(treeNode));
    }
  }

  /**
   * <p>
   *   Adds a child node (possibly containing its own child nodes) at the end of a child list.
   * </p>
   * @param nodeToAdd Node to insert at the end of a child list.
   * @return Returns this to enable chain calls.
   */
  BinomialTreeNode<K, V> addChildrenLast(BinomialTreeNode<K, V> nodeToAdd)
  {
    children.addLast(nodeToAdd);

    return this;
  }

  /**
   * <p>
   *   Converts this node to a heap entry storing key and value from this node.
   * </p>
   * @return IHeap.HeapEntry<K, V> with the same key and value as stored in this node.
   */
  IHeap.HeapEntry<K, V> toHeapEntry()
  {
    return new IHeap.HeapEntry<>(key, value);
  }

  /**
   * <p>
   *   Compares 2 nodes by a stored key, utilizing its compareTo() method.
   * </p>
   * @param other Other node to compare with.
   * @return negative value - this < other; 0 - this == other; positive value - this > other.
   */
  @Override
  public int compareTo(BinomialTreeNode<K, V> other)
  {
    return key.compareTo(other.key);
  }

  /**
   * @return Returns list of children of this node.
   */
  public LinkedList<BinomialTreeNode<K, V>> getChildren()
  {
    return children;
  }

  /**
   * @return Stored key.
   */
  public K getKey()
  {
    return key;
  }

  /**
   * @return Stored value.
   */
  public V getValue()
  {
    return value;
  }

  /**
   * <p>
   *   Key stored in this node.
   * </p>
   */
  private K key;
  /**
   * <p>
   *   Value stored in this node.
   * </p>
   */
  private V value;
  /**
   * <p>
   *   List of children of this node. Each child might have its own children.
   * </p>
   */
  private LinkedList<BinomialTreeNode<K, V>> children;
}
