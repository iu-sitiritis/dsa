import java.lang.reflect.Constructor;
import java.util.LinkedList;
import java.util.Iterator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tymur Lysenko. @sitiritis
 */
public class Main
{
  public static void simpleMaxQueueTest()
  {
    MaxQueue<Integer, String> q1 = new MaxQueue<>();
    MaxQueue<Integer, String> q2 = new MaxQueue<>();

    q1.insert(5, "Innopolis");
    q1.insert(4, "University");

    q2.insert(7, "It");
    q2.insert(5, "Works");
    q2.insert(11, "How?");

    q1.insert(2, "World");
    q1.insert(4, "Hello");

    q1.merge(q2);

    while (!q1.isEmpty())
    {
      IHeap.HeapEntry<Integer, String> curElement = q1.removeTop();
      System.out.println(curElement.getKey() + " " + curElement.getValue());
    }
  }

  public static void trickyMaxQueueTestCase()
  {
    MaxQueue<Integer, Integer> q1 = new MaxQueue<>();
    MaxQueue<Integer, Integer> q2 = new MaxQueue<>();

    q1.insert(50, 50);
    q1.insert(100, 100);
    q1.insert(75, 75);
    q1.insert(20, 20);
    q1.insert(17, 17);

    q2.insert(59, 59);
    q2.insert(72, 72);
    q2.insert(200, 200);
    q2.insert(38, 38);
    q2.insert(42, 42);
    q2.insert(0, 0);
    q2.insert(7, 7);

    q1.merge(q2);

    while (!q1.isEmpty())
    {
      IHeap.HeapEntry<Integer, Integer> curEntry = q1.removeMax();
      System.out.println(curEntry.getKey() + " " + curEntry.getValue());
    }
  }

  public static void cormenTestCase()
  {
    MinQueue<Integer, Integer> q1 = new MinQueue<>();
    MinQueue<Integer, Integer> q2 = new MinQueue<>();

    q1.insert(41, 41);
    q1.insert(28, 28);
    q1.insert(33, 33);
    q1.insert(15, 15);
    q1.insert(7, 7);
    q1.insert(25, 25);
    q1.insert(12, 12);

    q2.insert(55, 55);
    q2.insert(45, 45);
    q2.insert(32, 32);
    q2.insert(30, 30);
    q2.insert(24, 24);
    q2.insert(23, 23);
    q2.insert(22, 22);
    q2.insert(8, 8);
    q2.insert(50, 50);
    q2.insert(48, 48);
    q2.insert(29, 29);
    q2.insert(31, 31);
    q2.insert(17, 17);
    q2.insert(10, 10);
    q2.insert(44, 44);
    q2.insert(6, 6);
    q2.insert(27, 27);
    q2.insert(3, 3);
    q2.insert(37, 37);
    q2.insert(18, 18);

    q1.merge(q2);

    while (!q1.isEmpty())
    {
      IHeap.HeapEntry<Integer, Integer> curEntry = q1.removeMin();
      System.out.println(curEntry.getKey() + " " + curEntry.getValue());
    }
  }

  public static void todoList() throws ParseException
  {
    SortedMap<Date, MaxQueue<Integer, String>> todoList = new TreeMap<>();

    Pattern addCommandPat = Pattern.compile("^(\\d{2}\\.\\d{2}\\.\\d{2}) (.+?) (\\d+)");
    Pattern doCommandPat = Pattern.compile("^DO (\\d{2}\\.\\d{2}\\.\\d{2})");
    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
    Scanner input = new Scanner(System.in);

    int numCommands = Integer.parseInt(input.nextLine());

    // input & logic
    for (int i = 0; i < numCommands; ++i)
    {
      String curLine = input.nextLine();
      Matcher addMatcher = addCommandPat.matcher(curLine);

      if (addMatcher.find())
      {
        Date taskDate = dateFormat.parse(addMatcher.group(1));

        if (todoList.containsKey(taskDate))
        {
          todoList.get(taskDate).insert(
            Integer.parseInt(addMatcher.group(3)), // priority
            addMatcher.group(2) // task name
          );
        }
        else
        {
          MaxQueue<Integer, String> newQ = new MaxQueue<>();
          newQ.insert(
            Integer.parseInt(addMatcher.group(3)), // priority
            addMatcher.group(2) // task name
          );

          todoList.put(taskDate, newQ);
        }
      }
      else
      {
        Matcher doMatcher = doCommandPat.matcher(curLine);

        if (doMatcher.find())
        {
          Date taskDate = dateFormat.parse(doMatcher.group(1));

          if (todoList.containsKey(taskDate))
          {
            MaxQueue<Integer, String> dateToDoList = todoList.get(taskDate);

            if (!dateToDoList.isEmpty())
            {
              dateToDoList.removeMax();
            }
          }
        }
        else
        {
          System.out.println("Invalid input");
          break;
        }
      }
    }

    input.close();

    // copy all todo lists into one
    MaxQueue<Integer, String> totalTodoList = new MaxQueue<>();

    for (Map.Entry<Date, MaxQueue<Integer, String>> dateTodoListEntry : todoList.entrySet())
    {
      totalTodoList.merge(new MaxQueue<>(dateTodoListEntry.getValue()));
    }

    // output
    for (Map.Entry<Date, MaxQueue<Integer, String>> dateTodoListEntry : todoList.entrySet())
    {
      MaxQueue<Integer, String> dateTodoList = dateTodoListEntry.getValue();

      System.out.println("TODOList " + dateFormat.format(dateTodoListEntry.getKey()));

      while (!dateTodoList.isEmpty())
      {
        System.out.println("  " + dateTodoList.removeMax().getValue());
      }
    }

    System.out.println("TODOList");
    while (!totalTodoList.isEmpty())
    {
      System.out.println("  " + totalTodoList.removeMax().getValue());
    }
  }

  public static void main(String[] args) throws ParseException
  {
//    simpleMaxQueueTest();
//    trickyMaxQueueTestCase();
//    cormenTestCase();
    todoList();
  }
}

enum Priority
{
  MIN,
  MAX
}

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Heap abstract data type.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
interface IHeap<K extends Comparable<K>, V>
{
  /**
   * <p>
   *  Represents an entry in a heap. Needs not necessarily to be stored by an implementation. Used to be returned by
   *  methods defined in this ATD.
   * </p>
   * @param <K> Type of a stored key.
   * @param <V> Type of a stored value.
   */
  class HeapEntry<K extends Comparable, V>
  {
    /**
     * <p>
     *   Constructs a new heap entry with a given key and an associated value.
     * </p>
     * @param keyInit Value of a key stored in the object.
     * @param valueInit Value of a value stored in the object.
     */
    HeapEntry(K keyInit, V valueInit)
    {
      key = keyInit;
      value = valueInit;
    }

    public K getKey()
    {
      return key;
    }

    public V getValue()
    {
      return value;
    }

    /**
     * <p>
     *   Key stored in a heap. Uniqueness is defined by implementation.
     * </p>
     */
    private K key;
    /**
     * <p>
     *   Value associated to the key.
     * </p>
     */
    private V value;
  }

  /**
   * <p>
   *   Insert a new entry into a heap.
   * </p>
   * @param key Key to insert.
   * @param value Associated value to insert.
   * @return Returns this to enable chain calls.
   */
  IHeap<K, V> insert(K key, V value);

  /**
   * @return HeapEntry<K, V> stored on top of the heap without removing it.
   */
  HeapEntry<K, V> top();

  /**
   * @return HeapEntry<K, V> stored on top of the heap and removing it.
   */
  HeapEntry<K, V> removeTop();

  /**
   * <p>
   *   Removes all elements from heap without returning anything.
   * </p>
   */
  void clear();

  /**
   * @return Size of the current heap.
   */
  int size();

  /**
   * @return true if heap is empty, false otherwise.
   */
  default boolean isEmpty()
  {
    return size() <= 0;
  }
}

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Represents a meargeable heap ADT, that is a heap that can be merged with another heap.
 * </p>
 * @param <T> Type of an other heap that an implementer is able to take elements from.
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
interface IMergeableHeap<T extends IMergeableHeap<?, K, V>, K extends Comparable<K>, V> extends IHeap<K, V>
{
  /**
   * <p>
   *   Takes all elements from a given other heap and merges them into the current one, removing it from the other.
   * </p>
   * @param other Heap to take elements from.
   * @return Returns this to enable chain calls.
   */
  IMergeableHeap<T, K, V> merge(T other);
}

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Represents a node in a binomial tree.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
class BinomialTreeNode<K extends Comparable<K>, V> implements Comparable<BinomialTreeNode<K, V>>
{
  /**
   * <p>
   *   Constructs a new BinomialTreeNode<K, V> storing a given key and value and an empty list of child nodes.
   * </p>
   * @param keyInit Value of a key stored in the object.
   * @param valueInit Value of a value stored in the object.
   */
  public BinomialTreeNode(K keyInit, V valueInit)
  {
    key = keyInit;
    value = valueInit;
    children = new LinkedList<>();
  }

  /**
   * <p>
   *   Copy constructor. Constructs a new BinomialTreeNode<K, V> trying to recursively deep copy keys, values and
   *   children from an other given node using a copy constructor. If no copy constructor exists for K or V, just
   *   assigns references from the other.
   * </p>
   * @param other Node to copy from.
   */
  public BinomialTreeNode(BinomialTreeNode<K, V> other)
  {
    // try to deep copy key & value via reflection
    Class<?> keyClass = other.key.getClass();

    try
    {
      Constructor<?> keyCopyConstructor = keyClass.getConstructor(keyClass);
      key = (K) keyCopyConstructor.newInstance(other.key);
    }
    catch (Exception ex)
    {
      key = other.key;
    }

    Class<?> valueClass = other.value.getClass();

    try
    {
      Constructor<?> valueCopyConstructor = valueClass.getConstructor(valueClass);
      value = (V) valueCopyConstructor.newInstance(other.value);
    }
    catch (Exception ex)
    {
      value = other.value;
    }

    children = new LinkedList<>();
    for (BinomialTreeNode<K, V> treeNode : other.children)
    {
      children.addLast(new BinomialTreeNode<>(treeNode));
    }
  }

  /**
   * <p>
   *   Adds a child node (possibly containing its own child nodes) at the end of a child list.
   * </p>
   * @param nodeToAdd Node to insert at the end of a child list.
   * @return Returns this to enable chain calls.
   */
  BinomialTreeNode<K, V> addChildrenLast(BinomialTreeNode<K, V> nodeToAdd)
  {
    children.addLast(nodeToAdd);

    return this;
  }

  /**
   * <p>
   *   Converts this node to a heap entry storing key and value from this node.
   * </p>
   * @return IHeap.HeapEntry<K, V> with the same key and value as stored in this node.
   */
  IHeap.HeapEntry<K, V> toHeapEntry()
  {
    return new IHeap.HeapEntry<>(key, value);
  }

  /**
   * <p>
   *   Compares 2 nodes by a stored key, utilizing its compareTo() method.
   * </p>
   * @param other Other node to compare with.
   * @return negative value - this < other; 0 - this == other; positive value - this > other.
   */
  @Override
  public int compareTo(BinomialTreeNode<K, V> other)
  {
    return key.compareTo(other.key);
  }

  /**
   * @return Returns list of children of this node.
   */
  public LinkedList<BinomialTreeNode<K, V>> getChildren()
  {
    return children;
  }

  /**
   * @return Stored key.
   */
  public K getKey()
  {
    return key;
  }

  /**
   * @return Stored value.
   */
  public V getValue()
  {
    return value;
  }

  /**
   * <p>
   *   Key stored in this node.
   * </p>
   */
  private K key;
  /**
   * <p>
   *   Value stored in this node.
   * </p>
   */
  private V value;
  /**
   * <p>
   *   List of children of this node. Each child might have its own children.
   * </p>
   */
  private LinkedList<BinomialTreeNode<K, V>> children;
}

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Represents a binomial tree.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
class BinomialTree<K extends Comparable<K>, V> implements Comparable<BinomialTree<K, V>>
{
  /**
   * <p>
   *   Constructs a binomial tree with 1 node , storing a given key and value.
   * </p>
   * @param key Key to be stored in a node.
   * @param value Value to be stored in a node.
   * @param priorityInit Defines a behavior of the tree. If MIN - then a this tree is a minimal binomial tree, storing
   *                     the smallest element on the top. If MAX - then a this tree is a maximal binomial tree, storing
   *                     the largest element on the top.
   */
  public BinomialTree(K key, V value, Priority priorityInit)
  {
    root = new BinomialTreeNode<>(key, value);
    priority = priorityInit;
  }

  /**
   * <p>
   *   Copy constructor. Constructs a new binomial tree recursively copying all nodes from an other given binomial tree
   *   with their key and values utilizing BinomialTreeNode copy constructor. Also, copies other's priority.
   * </p>
   * @param other Other binomial tree to copy from.
   */
  public BinomialTree(BinomialTree<K, V> other)
  {
    priority = other.priority;
    root = new BinomialTreeNode<>(other.root);
  }

  /**
   * <p>
   *   Constructs a binomial tree from a given root with a given priority, assigning  It does not copy nodes from a
   *   given root node.
   * </p>
   * @param rootInit Root node to construct a tree from.
   * @param priorityInit Defines a behavior of the tree. If MIN - then a this thee is a minimal binomial tree, storing
   *                     the smallest element on the top. If MAX - then a this thee is a maximal binomial tree, storing
   *                     the largest element on the top.
   */
  private BinomialTree(BinomialTreeNode<K, V> rootInit, Priority priorityInit)
  {
    root = rootInit;
    priority = priorityInit;
  }

  /**
   * <p>
   *   Defines if this binomial tree can be merged with an other given binomial tree. Binomial trees can be merged if
   *   they are of the same size.
   * </p>
   * @param other Binomial tree to test if it can be used to link values from it into this tree.
   * @return true - trees can be merged; false otherwise.
   */
  public boolean canLink(BinomialTree<K, V> other)
  {
    return (other != null) && (getOrder() == other.getOrder());
  }

  /**
   * <p>
   *   Merges other binomial tree into the current, if this operation is legal, otherwise IllegalArgumentException is
   *   thrown. Other tree remains untouched.
   * </p>
   * <p>
   *   Running time - O(1).
   * </p>
   * @param other Tree to link into this tree.
   * @return Returns this to enable chain calls.
   */
  public BinomialTree<K, V> link(final BinomialTree<K, V> other)
  {
    if (canLink(other))
    {
      BinomialTreeNode<K, V> newRoot;
      BinomialTreeNode<K, V> rootToMerge;

      switch (priority)
      {
        case MAX:
        {
          if (root.compareTo(other.root) > 0)
          {
            newRoot = root;
            rootToMerge = other.root;
          }
          else
          {
            newRoot = other.root;
            rootToMerge = root;
          }

          break;
        }
        case MIN:
        {
          if (root.compareTo(other.root) < 0)
          {
            newRoot = root;
            rootToMerge = other.root;
          }
          else
          {
            newRoot = other.root;
            rootToMerge = root;
          }

          break;
        }
        default:
        {
          throw new IllegalStateException("Illegal state. Binomial tree can only be a minimal or maximal.");
        }
      }

      root = newRoot.addChildrenLast(rootToMerge);
    }
    else
    {
      throw new IllegalArgumentException("Cannot link binomial trees of different orders.");
    }

    return this;
  }

  /**
   * @return Order of this binomial tree.
   */
  public int getOrder()
  {
    return root.getChildren().size();
  }

  /**
   * @return Root of this binomial tree.
   */
  public BinomialTreeNode<K, V> getRoot()
  {
    return root;
  }

  /**
   * @return Priority of this binomial tree.
   */
  public Priority getPriority()
  {
    return priority;
  }

  /**
   * <p>
   *   Compares 2 binomial trees by their roots utilizing BinomialTreeNode.compareTo().
   * </p>
   * @param other Other tree to compare to this.
   * @return negative value - this < other; 0 - this == other; positive value - this > other.
   */
  @Override
  public int compareTo(BinomialTree<K, V> other)
  {
    return root.compareTo(other.root);
  }

  /**
   * @return List of children of a root node as a forest of binomial trees, root of each is a child node of this
   * binomial tree's root.
   */
  public LinkedList<BinomialTree<K, V>> childrenForest()
  {
    LinkedList<BinomialTree<K, V>> treesOfChildren = new LinkedList<>();

    for (BinomialTreeNode<K, V> rootOfTree : root.getChildren())
    {
      treesOfChildren.addLast(new BinomialTree<>(rootOfTree, priority));
    }

    return treesOfChildren;
  }

  /**
   * <p>
   *   Root of this binomial tree.
   * </p>
   */
  private BinomialTreeNode<K, V> root;
  /**
   * <p>
   *   Priority of this binomial tree. It defines the behavior of the tree. If MIN - then a this tree is a minimal
   *   binomial tree, storing the smallest element on the top. If MAX - then a this tree is a maximal binomial tree,
   *   storing the largest element on the top.
   * </p>
   */
  private Priority priority;
}

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Represents a binomial heap.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
class BinomialHeap<K extends Comparable<K>, V> implements IMergeableHeap<BinomialHeap<K, V>, K, V>
{
  /**
   * <p>
   *   Constructs an empty binomial heap with a given priority.
   * </p>
   * @param priorityInit Defines a behavior of the tree. If MIN - then a this thee is a minimal binomial tree, storing
   *                     the smallest element on the top. If MAX - then a this thee is a maximal binomial tree, storing
   *                     the largest element on the top.
   */
  public BinomialHeap(Priority priorityInit)
  {
    priority = priorityInit;
    trees = new LinkedList<>();
    size = 0;
  }

  /**
   * <p>
   *   Tries to deep copy other binomial heap. If a copy constructor is not supported by K or V, references are copied
   *   instead.
   * </p>
   * @param other Binomial heap to copy from.
   */
  public BinomialHeap(BinomialHeap<K, V> other)
  {
    size = other.size;
    priority = other.priority;

    trees = new LinkedList<>();
    for (BinomialTree<K, V> tree : other.trees)
    {
      trees.addLast(new BinomialTree<>(tree));
    }
  }

  /**
   * <p>
   *   Constructs a new binomial heap from a given list of binomial trees and a priority. Does not copy given trees into
   *   a new instance.
   * </p>
   * @param treesInit Trees to construct a binomial heap from.
   * @param priorityInit Defines a behavior of the tree. If MIN - then a this thee is a minimal binomial tree, storing
   *                     the smallest element on the top. If MAX - then a this thee is a maximal binomial tree, storing
   *                     the largest element on the top.
   */
  private BinomialHeap(LinkedList<BinomialTree<K, V>> treesInit, Priority priorityInit)
  {
    trees = treesInit;
    priority = priorityInit;
  }

  /**
   * <p>
   *   Merges all elements from the second binomial heap into this binomial heap, removing it form the original. If
   *   priorities are different or other is null - an exception is thrown.
   * </p>
   * <p>
   *   Since the algorithm iterates over all trees in the forests of both this and the other heap, which have maximal
   *   sizes 1 + log(size) & 1 + log(other.size) respectively, applying constant operations for each element and then
   *   calls a balance() method, running time of which will be O(2 + log(size) + log(other.size)) it's running time is:
   *   O(log(size) + log(other.size))
   * </p>
   * @param other Heap to take elements from.
   * @return Returns this to enable chain calls.
   */
  public BinomialHeap<K, V> merge(BinomialHeap<K, V> other)
  {
    if (other == null)
    {
      throw new NullPointerException("Binomial heap to link is null");
    }
    if (priority != other.priority)
    {
      throw new IllegalArgumentException("Minimal and maximal binomial heaps can not be merged");
    }
    else
    {
      LinkedList<BinomialTree<K, V>> newTrees = new LinkedList<>();
      LinkedList<BinomialTree<K, V>> biggerForest;
      LinkedList<BinomialTree<K, V>> smallerForest;

      if (trees.size() > other.trees.size())
      {
        biggerForest = trees;
        smallerForest = other.trees;
      }
      else
      {
        biggerForest = other.trees;
        smallerForest = trees;
      }

      while (!smallerForest.isEmpty())
      {
        BinomialTree<K, V> curTreeFirst = smallerForest.removeFirst();
        BinomialTree<K, V> curTreeSecond = biggerForest.removeFirst();

        if (curTreeFirst.getOrder() > curTreeSecond.getOrder())
        {
          newTrees.addLast(curTreeSecond);
          newTrees.addLast(curTreeFirst);
        }
        else
        {
          newTrees.addLast(curTreeFirst);
          newTrees.addLast(curTreeSecond);
        }
      }

      while (!biggerForest.isEmpty())
      {
        newTrees.addLast(biggerForest.removeFirst());
      }

      trees = newTrees;
      balance();
      size += other.size;
      other.size = 0;
      return this;
    }
  }

  /**
   * <p>
   *   Running time - O(log(size)), since that is the time taken by a balance() method, other statements are constants.
   * </p>
   * @param key Key to insert.
   * @param value Associated value to insert.
   * @return Returns this to enable chain calls.
   */
  @Override
  public BinomialHeap<K, V> insert(K key, V value)
  {
    trees.addFirst(new BinomialTree<>(key, value, priority));
    balance();
    ++size;

    return this;
  }

  /**
   * <p>
   *   Running time - O(log(size)), since number of trees in the heap is 1 + log2(size).
   * </p>
   * @return Returns a binomial tree, root of which is maximal or minimal, depending on a priority. Since this class is
   * a binomial heap the root will contain a global extremum among all stored elements.
   */
  private BinomialTree<K, V> topTree()
  {
    Iterator<BinomialTree<K, V>> treeRootIterator = trees.iterator();
    BinomialTree<K, V> top = (treeRootIterator.hasNext()) ? treeRootIterator.next() : null;

    switch (priority)
    {
      case MAX:
      {
        while (treeRootIterator.hasNext())
        {
          BinomialTree<K, V> curTree = treeRootIterator.next();

          if (curTree.compareTo(top) > 0)
          {
            top = curTree;
          }
        }

        break;
      }
      case MIN:
      {
        while (treeRootIterator.hasNext())
        {
          BinomialTree<K, V> curTree = treeRootIterator.next();

          if (curTree.compareTo(top) < 0)
          {
            top = curTree;
          }
        }

        break;
      }
      default:
      {
        throw new IllegalStateException("Illegal state. Binomial heap can only be a minimal or maximal.");
      }
    }

    return top;
  }

  /**
   * <p>
   *   Running time - O(log(size)), since number of trees in the heap is 1 + log2(size).
   * </p>
   */
  @Override
  public HeapEntry<K, V> top()
  {
    HeapEntry<K, V> topEntry = null;

    if (!isEmpty())
    {
      topEntry = topTree().getRoot().toHeapEntry();
    }

    return topEntry;
  }

  /**
   * <p>
   *   Returns minimal or maximal key-value pair, depending on priority and removes it from the heap. If heap is empty
   *   an exception is thrown.
   * </p>
   * <p>
   *   Since topTree() takes O(log(size)) time, and link takes O(max(log(size), log(other.size))), where other is a
   *   forest of all children of a tree, root node of which is extremum, therefore other.size < size, hence running time
   *   is:
   *   O(log(size))
   * </p>
   */
  @Override
  public HeapEntry<K, V> removeTop()
  {
    if (isEmpty())
    {
      throw new IllegalStateException("Binomial heap is empty");
    }
    else
    {
      BinomialTree<K, V> curTopTree = topTree();
      trees.remove(curTopTree);
      --size;

      merge(new BinomialHeap<>(curTopTree.childrenForest(), priority));

      return curTopTree.getRoot().toHeapEntry();
    }
  }

  @Override
  public void clear()
  {
    trees.clear();
    size = 0;
  }


  /**
   * <p>
   *   At certain point in time it might be the case, when there are multiple binomial trees of the same order, then
   *   they must be merged together until there are no such trees in the forest. It iterates from left to right through
   *   all binomial trees and tries to link them, if it is possible, until no trees with different orders are left in
   *   the forest.
   * </p>
   * <p>
   *   Since the method iterates binomial forest once and for binomial heaps consisting of size nodes, maximal number of
   *   binomial trees is 1 + log2(size), running time is:
   *   O(log(size)).
   * </p>
   */
  private void balance()
  {
    if (!trees.isEmpty())
    {
      LinkedList<BinomialTree<K, V>> newTrees = new LinkedList<>();
      Iterator<BinomialTree<K, V>> treesIterator = trees.iterator();
      boolean proceedBalance = treesIterator.hasNext();
      BinomialTree<K, V> firstTree = null;

      if (proceedBalance)
      {
        firstTree = treesIterator.next();
      }

      while (proceedBalance)
      {
        if (treesIterator.hasNext())
        {
          BinomialTree<K, V> curTree = treesIterator.next();

          if (firstTree.canLink(curTree))
          {
            firstTree.link(curTree);
          }
          else if (firstTree.getOrder() > curTree.getOrder())
          {
            newTrees.addLast(curTree);
          }
          else
          {
            newTrees.addLast(firstTree);
            firstTree = curTree;
          }
        }
        else
        {
          newTrees.addLast(firstTree);
          proceedBalance = false;
        }

        // loop invariant
        if (newTrees.size() > 1)
        {
          assert
            newTrees.getLast().getOrder() > newTrees.get(newTrees.size() - 2).getOrder()
            : "Balance loop invariant is violated!";
        }
      }

      trees = newTrees;
    }
  }


  /**
   * @return Number of elements stored in a heap.
   */
  @Override
  public int size()
  {
    return size;
  }

  /**
   * @return Priority of this binomial tree.
   */
  public Priority getPriority()
  {
    return priority;
  }

  /**
   * <p>
   *   Forest of binomial trees sorted by tree order. The sort order is managed by this class.
   * </p>
   * <p>
   *   For binomial heaps consisting of size nodes, number of binomial trees is at most
   *   <a href="https://en.wikipedia.org/wiki/Binomial_heap">1 + log2(size)</a>.
   * </p>
   */
  private LinkedList<BinomialTree<K, V>> trees;
  /**
   * <p>
   *   Priority of this binomial heap. It defines the behavior of the heap. If MIN - then a this heap is a minimal
   *   binomial heap, i. e. the top element is the smallest. If MAX - then a this heap is a maximal binomial heap,
   *   i. e. the top element is the largest.
   * </p>
   */
  private Priority priority;
  /**
   * Number of elements stored in the binomial heap.
   */
  private int size;
}

/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Maximal priority queue.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
class MaxQueue<K extends Comparable<K>, V> extends BinomialHeap<K, V>
{
  /**
   * <p>
   *   Constructs an empty priority queue.
   * </p>
   */
  public MaxQueue()
  {
    super(Priority.MAX);
  }

  /**
   * <p>
   *   Tries to deep copy other priority queue. If a copy constructor is not supported by K or V, references are copied
   *   instead.
   * </p>
   * @param other Priority queue to copy from.
   */
  public MaxQueue(MaxQueue<K, V> other)
  {
    super((BinomialHeap<K, V>)other);
  }

  /**
   * @return Returns maximal key and associated value stored in the queue without removing it.
   */
  public HeapEntry<K, V> max()
  {
    return top();
  }

  /**
   * @return Returns maximal key and associated value stored in the queue and removes it.
   */
  public HeapEntry<K, V> removeMax()
  {
    return removeTop();
  }
}


/**
 * @author Tymur Lysenko. @sitiritis
 * <p>
 *   Minimal priority queue.
 * </p>
 * @param <K> Type of a stored key.
 * @param <V> Type of a stored value.
 */
class MinQueue<K extends Comparable<K>, V> extends BinomialHeap<K, V>
{
  /**
   * <p>
   *   Constructs an empty priority queue.
   * </p>
   */
  public MinQueue()
  {
    super(Priority.MIN);
  }

  /**
   * <p>
   *   Tries to deep copy other priority queue. If a copy constructor is not supported by K or V, references are copied
   *   instead.
   * </p>
   * @param other Priority queue to copy from.
   */
  public MinQueue(MinQueue<K, V> other)
  {
    super((BinomialHeap<K, V>)other);
  }

  /**
   * @return Returns minimal key and associated value stored in the queue without removing it.
   */
  public HeapEntry<K, V> min()
  {
    return top();
  }

  /**
   * @return Returns minimal key and associated value stored in the queue and removes it.
   */
  public HeapEntry<K, V> removeMin()
  {
    return removeTop();
  }
}
