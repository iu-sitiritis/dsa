package edu.innopolis;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main
{
    private static <T> void swap(List<T> list, int index1, int index2)
    {
        T temp = list.get(index1);
        list.set(index1, list.get(index2));
        list.set(index2, temp);
    }

    private static <T extends Comparable<T>> int maxElementIdxOfSubList(List<T> list, int start)
    {
        int maxIdx = start;
        T maxEl = list.get(maxIdx);

        for(int i = start + 1; i < list.size(); ++i)
        {
            if (list.get(i).compareTo(maxEl) > 0)
            {
                maxIdx = i;
                maxEl = list.get(i);
            }
        }

        return maxIdx;
    }

    private static <T extends Comparable<T>> void selctionSort(List<T> list)
    {
        for (int i = 0; i < list.size(); ++i)
        {
            int maxIdx = maxElementIdxOfSubList(list, i);

            if (i != maxIdx)
            {
                swap(list, i, maxIdx);
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException
    {
        Scanner input = new Scanner(new FileInputStream("input.txt"));
        PrintStream output = new PrintStream(new FileOutputStream("output.txt"));

        int size = input.nextInt();
        ArrayList<Integer> list = new ArrayList<Integer>(size);

        for (int i = 0; i < size; ++i)
        {
            list.add(input.nextInt());
        }

        selctionSort(list);
        for (int i = 0; i < size; ++i)
        {
            output.print(list.get(i));
            output.print(' ');
        }

        input.close();
        output.close();
    }
}
