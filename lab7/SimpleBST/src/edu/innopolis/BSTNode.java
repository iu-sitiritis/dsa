package edu.innopolis;

public class BSTNode<K, V>
{
  public BSTNode(K key, V value, int index, BSTNode<K, V> left, BSTNode<K, V> right)
  {
    this.key = key;
    this.value = value;
    this.index = index;
    this.left = left;
    this.right = right;
  }

  public K getKey()
  {
    return key;
  }

  public V getValue()
  {
    return value;
  }

  public int getIndex()
  {
    return index;
  }

  public BSTNode<K, V> getLeft()
  {
    return left;
  }

  public BSTNode<K, V> getRight()
  {
    return right;
  }


  public void setValue(V value)
  {
    this.value = value;
  }

  public void setIndex(int index)
  {
    this.index = index;
  }

  public void setLeft(BSTNode<K, V> left)
  {
    this.left = left;
  }

  public void setRight(BSTNode<K, V> right)
  {
    this.right = right;
  }

  private K key;
  private V value;
  private int index;
  private BSTNode<K, V> left;
  private BSTNode<K, V> right;
}
