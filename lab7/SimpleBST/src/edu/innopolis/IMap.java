package edu.innopolis;

public interface IMap<K, V>
{
  V find(K key);
  void add(K key, V value);
  default int getSize()
  {
    return size;
  }
  default boolean isEmpty()
  {
    return size == 0;
  }


  int size = 0;
}
