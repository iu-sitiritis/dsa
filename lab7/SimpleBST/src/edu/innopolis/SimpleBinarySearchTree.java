package edu.innopolis;

import javax.swing.plaf.basic.BasicToolBarSeparatorUI;

public class SimpleBinarySearchTree<K extends Comparable<K>, V> implements IMap<K, V>
{
  public SimpleBinarySearchTree()
  {
    root = null;
    size = 0;
  }

  @Override
  public V find(K key)
  {
    return null;
  }

  @Override
  public void add(K key, V value)
  {
    BSTNode<K, V> newNode = new BSTNode<>(key, value, size, null, null);

    if (isEmpty())
    {
      root = newNode;
    }
    else
    {
      BSTNode<K, V> curNode = root;
      boolean searchingNode = true;

      while (searchingNode)
      {
        if (key.compareTo(curNode.getKey()) < 0)
        {
          if (curNode.getLeft() == null)
          {
            curNode.setLeft(newNode);
            searchingNode = false;
          }
          else
          {
            curNode = curNode.getLeft();
          }
        }
        else
        {
          if (curNode.getRight() == null)
          {
            curNode.setRight(newNode);
            searchingNode = false;
          }
          else
          {
            curNode = curNode.getRight();
          }
        }
      }
    }

    ++size;
  }

  BSTNode<K, V> root;
  private int size;
}
