package edu.innopolis;

import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        LinkedHashMap<String, Integer> leftTextWords = new LinkedHashMap<>();
        LinkedHashMap<String, Integer> rightTextWords = new LinkedHashMap<>();

        Scanner inputScanner = new Scanner(System.in);

        int numWords = inputScanner.nextInt();
        for (int i = 0; i < numWords; ++i)
        {
            String curWord = inputScanner.next();

            if (leftTextWords.containsKey(curWord))
            {
                leftTextWords.replace(curWord, leftTextWords.get(curWord) + 1);
            }
            else
            {
                leftTextWords.put(curWord, 1);
            }
        }

        numWords = inputScanner.nextInt();
        for (int i = 0; i < numWords; ++i)
        {
            String curWord = inputScanner.next();

            if (rightTextWords.containsKey(curWord))
            {
                rightTextWords.replace(curWord, rightTextWords.get(curWord) + 1);
            }
            else
            {
                rightTextWords.put(curWord, 1);
            }
        }
        inputScanner.close();

        for (String word : leftTextWords.keySet())
        {
            if (rightTextWords.containsKey(word))
            {
                rightTextWords.replace(word, rightTextWords.get(word) - leftTextWords.get(word));

                if (rightTextWords.get(word) <= 0)
                {
                    rightTextWords.remove(word);
                }
            }
        }

        PrintStream output = new PrintStream(System.out);

        output.println(rightTextWords.size());

        for (String word : rightTextWords.keySet())
        {
            output.println(word);
        }

        output.close();
    }
}
