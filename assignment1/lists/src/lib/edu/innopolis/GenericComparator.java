package lib.edu.innopolis;

import java.util.Comparator;

// Comparator for any comparable type
class GenericComparator<T extends Comparable<T>> implements Comparator<T>
{
    @Override
    public int compare(T left, T right)
    {
        return left.compareTo(right);
    }
}
