package lib.edu.innopolis;

public interface IExtendedList<T> extends IList<T>
{
    // Returns true when leftIdx & rightIdx are valid indexes and leftIdx is <= rightIdx
    default boolean isRangeValid(int leftIdx, int rightIdx)
    {
        return ((isIndexValid(leftIdx) && isIndexValid(rightIdx)) && (leftIdx <= rightIdx));
    }
    // Returns true if index is valid, that is: strictly less than size and non-negative
    default boolean isIndexValid(int idx)
    {
        return (idx < size()) && (idx >= 0);
    }
    // Returns true when the list is not empty and the idx is last
    default boolean isIndexLast(int idx)
    {
        return (!isEmpty() && (idx == lastIndex()));
    }
    // Returns the index of the last element from the list, that is (size() - 1)
    // If list is empty, returns -1
    default int lastIndex()
    {
        return (size() - 1);
    }
}
