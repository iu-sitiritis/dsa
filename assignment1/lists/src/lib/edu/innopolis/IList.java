package lib.edu.innopolis;

import java.util.Comparator;

// List ADT
// Provides signatures for essential methods of any list
public interface IList<T>
{
    // Get size of the list
    int size();
    // True - list has no items, false - list has at least 1 item
    default boolean isEmpty()
    {
        return size() <= 0;
    }

    // Add item to the specified index idx
    void add(int idx, T item);
    // Add item at index 0 (in the front of the array)
    void addFirst(T item);
    // Add item at index size() - 1 (at the end of the array)
    void addLast(T item);

    // Delete item at index idx from the list
    void delete(int idx);
    // Delete the first item from the list
    void deleteFirst();
    // Delete the last item from the list
    void deleteLast();
    // Delete all items from the list
    void clear();
    // Delete the first occurrence of the item from the list
    void deleteFirstItem(T item);
    // Delete all occurrences of the item from the list
    void deleteAllItems(T item);

    // Returns true if item is in the list, otherwise returns false. Comparison is performed via item.equals()
    boolean isItemInList(T item);
    // Returns true if item is in the list, otherwise returns false. Comparison is performed via the comparator
    boolean isItemInList(T item, Comparator<T> comparator);
    // Returns first occurrence of the item, if item is not in the list - returns -1.
    // Comparison is performed via item.equals()
    int findFirst(T item);
    // Returns first occurrence of the item, if item is not in the list - returns -1.
    // Comparison is performed via the comparator
    int findFirst(T item, Comparator<T> comparator);

    // Set item at index idx
    void set(int idx, T item);
    // Get idx-th item of the list
    T get(int idx);
}
