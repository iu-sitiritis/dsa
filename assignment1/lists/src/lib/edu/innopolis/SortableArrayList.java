package lib.edu.innopolis;

public class SortableArrayList<T extends Comparable<T>> extends ArrayList<T> implements ISortableList<T>
{
    public SortableArrayList()
    {
        super();
    }

    public SortableArrayList(int initCapacity)
    {
        super(initCapacity);
    }

    public SortableArrayList(int initCapacity, int resizeFactorVal)
    {
        super(initCapacity, resizeFactorVal);
    }
}
