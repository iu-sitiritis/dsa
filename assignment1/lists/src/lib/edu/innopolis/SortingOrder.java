package lib.edu.innopolis;

public enum SortingOrder
{
    ASCENDING,
    DESCENDING
}
