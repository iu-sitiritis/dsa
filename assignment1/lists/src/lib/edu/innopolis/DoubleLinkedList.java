package lib.edu.innopolis;

import java.util.Comparator;

public class DoubleLinkedList<T> implements IExtendedList<T>
{
    // Default constructor: initializes an empty double linked list
    public DoubleLinkedList()
    {
        size = 0;
        head = null;
        tail = null;
        lastAccessedNode = null;
        lastAccessedNodeIdx = -1;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty()
    {
        return head == null;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void add(int idx, T item)
    {
        if (isIndexValid(idx))
        {
            if (idx == 0)
            {
                addFirst(item);
            }
            else if (idx == lastIndex())
            {
                addLast(item);
            }
            else // if index is not the first and not the last
            {
                DoubleLinkedListNode<T> oldNodeAtIdx = getNode(idx);
                DoubleLinkedListNode<T> prevNode = oldNodeAtIdx.getPrevious();

                lastAccessedNode = new DoubleLinkedListNode<>(item, prevNode, oldNodeAtIdx);

                prevNode.setNext(lastAccessedNode);
                oldNodeAtIdx.setPrevious(lastAccessedNode);
                ++size;
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void addFirst(T item)
    {
        lastAccessedNode = new DoubleLinkedListNode<>(item, null, head);

        if (tail == null)
        {
            tail = lastAccessedNode;
        }
        else
        {
            head.setPrevious(lastAccessedNode);
        }

        head = lastAccessedNode;

        ++size;
        lastAccessedNodeIdx = 0;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void addLast(T item)
    {
        lastAccessedNode = new DoubleLinkedListNode<>(item, tail, null);

        if (head == null)
        {
            head = lastAccessedNode;
        }
        else
        {
            tail.setNext(lastAccessedNode);
        }

        tail = lastAccessedNode;

        ++size;
        lastAccessedNodeIdx = lastIndex();
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void delete(int idx)
    {
        if (idx == 0)
        {
            deleteFirst();
        }
        else if (idx == lastIndex())
        {
            deleteLast();
        }
        else // if index is not the first and not the last
        {
            DoubleLinkedListNode<T> oldNodeAtIdx = getNode(idx);
            DoubleLinkedListNode<T> prevNode = oldNodeAtIdx.getPrevious();

            lastAccessedNode = oldNodeAtIdx.getNext().setPrevious(prevNode);

            prevNode.setNext(lastAccessedNode);
            --size;
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void deleteFirst()
    {
        if (isEmpty())
        {
            throw new IndexOutOfBoundsException();
        }
        else if (size == 1)
        {
            clear();
        }
        else
        {
            head = head.getNext();
            lastAccessedNode = head;
            lastAccessedNodeIdx = 0;
            --size;
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void deleteLast()
    {
        if (isEmpty())
        {
            throw new IndexOutOfBoundsException();
        }
        else if (size == 1)
        {
            clear();
        }
        else
        {
            tail = tail.getPrevious();
            lastAccessedNode = tail;
            lastAccessedNodeIdx = lastIndex();
            --size;
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void clear()
    {
        head = null;
        tail = null;
        size = 0;
        lastAccessedNode = null;
        lastAccessedNodeIdx = -1;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void deleteFirstItem(T item)
    {
        int foundItemIdx = findFirst(item);

        if (isIndexValid(foundItemIdx))
        {
            delete(foundItemIdx);
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void deleteAllItems(T item)
    {
        int foundItemIdx = findFirst(item);

        while (isIndexValid(foundItemIdx))
        {
            delete(foundItemIdx);
            foundItemIdx = findFirst(item);
        }
    }

    // Reset lastAccessedNode and its index
    public void resetLastAccessedNode()
    {
        lastAccessedNode = tail;
        lastAccessedNodeIdx = lastIndex();
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public boolean isItemInList(T item)
    {
        return findFirstNode(item) != null;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public boolean isItemInList(T item, Comparator<T> comparator)
    {
        return findFirstNode(item, comparator) != null;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public int findFirst(T item)
    {
        return (findFirstNode(item) == null) ? -1 : lastAccessedNodeIdx;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public int findFirst(T item, Comparator<T> comparator)
    {
        return (findFirstNode(item, comparator) == null) ? -1 : lastAccessedNodeIdx;
    }

    // Returns the first node with its value.equals(item),
    // and sets lastAccessedNode to the found node & its lastAccessedNodeIdx
    // If item is not in the list - returns null & not modifies lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> findFirstNode(T item)
    {
        DoubleLinkedListNode<T> resultItem = head;
        int accessedNodeIdx = (isEmpty()) ? -1 : 0;

        while ((resultItem != null) && !(resultItem.getValue().equals(item)))
        {
            resultItem = resultItem.getNext();
            ++accessedNodeIdx;
        }

        if (resultItem != null)
        {
            lastAccessedNode = resultItem;
            lastAccessedNodeIdx = accessedNodeIdx;
        }

        return resultItem;
    }

    // Returns the first node with its value == item, compared by comparator,
    // and sets lastAccessedNode to the found node & its lastAccessedNodeIdx
    // If item is not in the list - returns null & not modifies lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> findFirstNode(T item, Comparator<T> comparator)
    {
        DoubleLinkedListNode<T> resultItem = head;
        int accessedNodeIdx = (isEmpty()) ? -1 : 0;

        while ((resultItem != null) && (comparator.compare(resultItem.getValue(), item) != 0))
        {
            resultItem = resultItem.getNext();
            ++accessedNodeIdx;
        }

        if (resultItem != null)
        {
            lastAccessedNode = resultItem;
            lastAccessedNodeIdx = accessedNodeIdx;
        }

        return resultItem;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void set(int idx, T item)
    {
        getNode(idx).setValue(item);
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public T get(int idx)
    {
        return getNode(idx).getValue();
    }

    public int getLastAccessedIdx()
    {
        return lastAccessedNodeIdx;
    }

    // Returns node that is situated at index idx
    // Sets lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> getNode(int idx)
    {
        if (isIndexValid(idx))
        {
            int distanceFromHead = idx;
            int distanceFromLastAccessed = lastAccessedNodeIdx - idx;
            int distanceFromTail = lastIndex() - idx;

            // TODO: fix condition
            if (
                    Math.abs(distanceFromLastAccessed) <= distanceFromHead &&
                    Math.abs(distanceFromLastAccessed) <= distanceFromTail
            )
            // if distance last accessed node is closer to idx, than head and tail
            {
                return traverseFromTo(lastAccessedNode, lastAccessedNodeIdx, idx);
            }
            else if (distanceFromHead <= distanceFromTail)
            {
                return traverseFromTo(head, 0, idx);
            }
            else
            {
                return traverseFromTo(tail, lastIndex(), idx);
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // Based on the position of fromNodeIdx & toIdx, traverses the double linked list in an appropriate direction
    protected DoubleLinkedListNode<T> traverseFromTo(DoubleLinkedListNode<T> fromNode, int fromNodeIdx, int toIdx)
    {
        if (isRangeValid(toIdx, fromNodeIdx))
        {
            return traverseLeftFromTo(fromNode, fromNodeIdx, toIdx);
        }
        else if (isRangeValid(fromNodeIdx, toIdx))
        {
            return traverseRightFromTo(fromNode, fromNodeIdx, toIdx);
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // Traverses double linked list starting @ fromNode, which index is fromNodeIdx in the list, until toIdx
    // moving in the left direction
    // Sets lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> traverseLeftFromTo(DoubleLinkedListNode<T> fromNode, int fromNodeIdx, int toIdx)
    {
        if (isRangeValid(toIdx, fromNodeIdx))
        {
            lastAccessedNode = fromNode;

            for (lastAccessedNodeIdx = fromNodeIdx; lastAccessedNodeIdx > toIdx; --lastAccessedNodeIdx)
            {
                if (lastAccessedNode != null)
                {
                    lastAccessedNode = lastAccessedNode.getPrevious();
                }
                else
                {
                    resetLastAccessedNode();
                    throw new IllegalArgumentException("Given node index is not the correct index of the node");
                }
            }

            return lastAccessedNode;
        }
        else
        {
            throw new IllegalArgumentException(
                    "Cannot reach index " + toIdx + " when moving from " + fromNodeIdx + " index in the left direction"
            );
        }
    }

    // Traverses double linked list starting @ fromNode, which index is fromNodeIdx in the list, until toIdx
    // moving in the right direction
    // Sets lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> traverseRightFromTo(DoubleLinkedListNode<T> fromNode, int fromNodeIdx, int toIdx)
    {
        if (isRangeValid(fromNodeIdx, toIdx))
        {
            lastAccessedNode = fromNode;

            for (lastAccessedNodeIdx = fromNodeIdx; lastAccessedNodeIdx < toIdx; ++lastAccessedNodeIdx)
            {
                if (lastAccessedNode != null)
                {
                    lastAccessedNode = lastAccessedNode.getNext();
                }
                else
                {
                    resetLastAccessedNode();
                    throw new IllegalArgumentException("Given node index is not the correct index of the node");
                }
            }

            return lastAccessedNode;
        }
        else
        {
            throw new IllegalArgumentException(
                    "Cannot reach index " + toIdx + " when moving from " + fromNodeIdx + " index in the right direction"
            );
        }
    }

    // Number of items inserted into the list
    protected int size;
    // Reference of the first node in the list
    // If no elements in the list is null
    protected DoubleLinkedListNode<T> head;
    // Reference of the last node in the list
    // If no elements in the list is null
    protected DoubleLinkedListNode<T> tail;
    // Reference of the last accessed node to utilize the Principle of Locality
    // If no elements in the list is null
    protected DoubleLinkedListNode<T> lastAccessedNode;
    // Index of the last accessed node
    // If no elements in the list == -1
    protected int lastAccessedNodeIdx;
}
