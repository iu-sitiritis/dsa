package lib.edu.innopolis;

// Mutable node of double linked list
public class DoubleLinkedListNode<T>
{
    // Constructs an independent (not linked to any other node) node, setting the initial value
    public DoubleLinkedListNode(T initValue)
    {
        value = initValue;
        next = null;
        previous = null;
    }

    // Constructs a node with initial value and next & previous references
    public DoubleLinkedListNode(T initValue, DoubleLinkedListNode<T> initPrevious, DoubleLinkedListNode<T> initNext)
    {
        value = initValue;
        previous = initPrevious;
        next = initNext;
    }

    // Copy constructor
    public DoubleLinkedListNode(DoubleLinkedListNode<T> otherNode)
    {
        value = otherNode.getValue();
        next = otherNode.getNext();
        previous = otherNode.getPrevious();
    }

    public DoubleLinkedListNode<T> getNext()
    {
        return next;
    }

    public DoubleLinkedListNode<T> setNext(DoubleLinkedListNode<T> next)
    {
        this.next = next;
        return this;
    }

    public DoubleLinkedListNode<T> getPrevious()
    {
        return previous;
    }

    public DoubleLinkedListNode<T> setPrevious(DoubleLinkedListNode<T> previous)
    {
        this.previous = previous;
        return this;
    }

    public T getValue()
    {
        return value;
    }

    public DoubleLinkedListNode<T> setValue(T value)
    {
        this.value = value;
        return this;
    }

    // Reference of the next node
    private DoubleLinkedListNode<T> next;
    // Reference of the previous node
    private DoubleLinkedListNode<T> previous;
    // Value stored by the node
    private T value;
}
