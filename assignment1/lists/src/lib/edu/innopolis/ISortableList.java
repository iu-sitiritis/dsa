package lib.edu.innopolis;

import java.util.Comparator;
import java.util.Optional;

// Interface that represents a sortable list of comparable items
// It provides signature and default implementations for sorting lists
// The default implemented algorithm is insertion sort
public interface ISortableList<T extends Comparable<T>> extends IList<T>
{
    // Sort data of the implementer in sortOrderOption sort order
    // By default sort order is SortingOrder.ASCENDING
    default void sort(Optional<SortingOrder> sortOrderOption)
    {
        sort(this, new GenericComparator<T>(), sortOrderOption);
    }

    // Sort data of the implementer using comparator in sortOrderOption sort order
    // By default sort order is SortingOrder.ASCENDING
    default void sort(Comparator<T> comparator, Optional<SortingOrder> sortOrderOption)
    {
        sort(this, comparator, sortOrderOption);
    }

    // Static method to sort list using comparator in sortOrderOption.
    // By default sort order is SortingOrder.ASCENDING
    // The implementation is insertion sort
    static <T> void sort(IList<T> list, Comparator<T> comparator, Optional<SortingOrder> sortOrderOption)
    {
        final int compareFactor =
                (sortOrderOption.isPresent() && sortOrderOption.get() == SortingOrder.DESCENDING) ? -1 : 1;

        for (int i = 1; i < list.size(); ++i)
        {
            T curItem = list.get(i);

            int j = i - 1;
            while ((j >= 0) && (compareFactor * comparator.compare(curItem, list.get(j)) < 0))
            {
                list.set(j + 1, list.get(j));
                --j;
            }

            list.set(j + 1, curItem);
        }
    }
}
