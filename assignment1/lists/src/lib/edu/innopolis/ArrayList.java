package lib.edu.innopolis;

import java.util.Comparator;

public class ArrayList<T> implements IExtendedList<T>
{
    // Default constructor
    public ArrayList()
    {
        size = 0;
        resizeFactor = DEFAULT_RESIZE_FACTOR;
        capacity = DEFAULT_CAPACITY;
        array = (T[]) new Object[capacity];
    }

    // Constructor with initial capacity of the array
    public ArrayList(int initCapacity)
    {
        if (initCapacity >= MIN_CAPACITY)
        {
            capacity = initCapacity;
        }
        else
        {
            capacity = DEFAULT_CAPACITY;
            throw new IllegalArgumentException("The capacity must be at least. Setting it to default - " +
                    Integer.toString(DEFAULT_CAPACITY));
        }

        size = 0;
        resizeFactor = DEFAULT_RESIZE_FACTOR;
        array = (T[]) new Object[capacity];
    }

    // Constructor with initial capacity of the array and its resize factor
    public ArrayList(int initCapacity, int resizeFactorVal)
    {
        if (resizeFactorVal >= MIN_RESIZE_FACTOR)
        {
            resizeFactor = resizeFactorVal;
        }
        else
        {
            resizeFactor = DEFAULT_RESIZE_FACTOR;
            throw new IllegalArgumentException("The resize factor must be at least 2. Setting it to default - " +
                    Integer.toString(DEFAULT_RESIZE_FACTOR));
        }

        if (initCapacity >= MIN_CAPACITY)
        {
            capacity = initCapacity;
        }
        else
        {
            capacity = DEFAULT_CAPACITY;
            throw new IllegalArgumentException("The capacity must be at least. Setting it to default - " +
                    Integer.toString(DEFAULT_CAPACITY));
        }

        size = 0;
        array = (T[]) new Object[capacity];
    }

    // Returns the value of the resizeFactor
    public int getResizeFactor()
    {
        return resizeFactor;
    }

    // Returns the value of the capacity
    public int getCapacity()
    {
        return capacity;
    }

    @Override
    public int size()
    {
        return size;
    }

    @Override
    public boolean isEmpty()
    {
        return size == 0;
    }

    @Override
    public boolean isRangeValid(int leftIdx, int rightIdx)
    {
        return ((isIndexValid(leftIdx) && isIndexValid(rightIdx)) && (leftIdx <= rightIdx));
    }

    @Override
    public boolean isIndexValid(int idx)
    {
        return (idx < size) && (idx >= 0);
    }

    @Override
    public boolean isIndexLast(int idx)
    {
        return (!isEmpty() && (idx == (size - 1)));
    }

    // Returns true if the array with newCapacity will be able to store all items it already has. False otherwise
    public boolean canResize(int newCapacity)
    {
        return (newCapacity >= size) && (newCapacity >= MIN_CAPACITY);
    }

    // Resizes the actual array capacity to the newCapacity
    public void resize(int newCapacity)
    {
        if (canResize(newCapacity))
        {
            T[] new_array = (T[]) new Object[newCapacity];
            System.arraycopy(array, 0, new_array, 0, array.length);
            array = new_array;
            capacity = newCapacity;
        }
        else
        {
            throw new IllegalArgumentException(
                    "Cannot resize the list. New capacity is less then the size of the list or less then " +
                            MIN_CAPACITY
            );
        }
    }

    @Override
    public void add(int idx, T item)
    {
        if (isIndexValid(idx))
        {
            resizeIfNecessary();
            probeItemsRight(idx, 1);
            array[idx] = item;
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void addFirst(T item)
    {
        resizeIfNecessary();
        probeItemsRight(0, 1);
        array[0] = item;
    }

    @Override
    public void addLast(T item)
    {
        resizeIfNecessary();
        array[size++] = item;
    }

    @Override
    public void delete(int idx)
    {
        if (isIndexValid(idx))
        {
            if (isIndexLast(idx))
            {
                deleteLast();
            }
            else
            {
                probeItemsLeft(idx, idx + 1);
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void deleteFirstItem(T item)
    {
        int deleteIdx = findFirst(item);

        if (isIndexValid(deleteIdx))
        {
            delete(deleteIdx);
        }
    }

    @Override
    public void deleteAllItems(T item)
    {
        int deleteIdx = -1;

        while ( isIndexValid(deleteIdx = findFirst(item)) )
        {
            delete(deleteIdx);
        }
    }

    @Override
    public void deleteFirst()
    {
        if (isEmpty())
        {
            throw new IndexOutOfBoundsException();
        }
        else
        {
            if (size > 1)
            {
                probeItemsLeft(0, 1);
            }
            else
            {
                deleteLast();
            }
        }
    }

    @Override
    public void deleteLast()
    {
        if (isEmpty())
        {
            throw new IndexOutOfBoundsException();
        }
        else
        {
            --size;
        }
    }

    @Override
    public void clear()
    {
        size = 0;
    }

    // Deletes all elements from the list and resizes it to newCapacity
    public void clear(int newCapacity)
    {
        size = 0;
        resize(newCapacity);
    }

    @Override
    public boolean isItemInList(T item)
    {
        return isIndexValid(findFirst(item));
    }

    @Override
    public boolean isItemInList(T item, Comparator<T> comparator)
    {
        return isIndexValid(findFirst(item, comparator));
    }

    @Override
    public int findFirst(T item)
    {
        int resultIdx = -1;

        for (int i = 0; i < size; ++i)
        {
            if (item.equals(array[i]))
            {
                resultIdx = i;
                break;
            }
        }

        return resultIdx;
    }

    @Override
    public int findFirst(T item, Comparator<T> comparator)
    {
        int resultIdx = -1;

        for (int i = 0; i < size; ++i)
        {
            if (comparator.compare(item, array[i]) == 0)
            {
                resultIdx = i;
                break;
            }
        }

        return resultIdx;
    }

    @Override
    public void set(int idx, T item)
    {
        if (isIndexValid(idx))
        {
            array[idx] = item;
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public T get(int idx)
    {
        if (isIndexValid(idx))
        {
            return array[idx];
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // When called checks if the size of the array already reached its capacity.
    // If that is the case extends its capacity
    protected void resizeIfNecessary()
    {
        if (size == capacity)
        {
            resize(capacity * resizeFactor);
        }
    }

    // Moves all items starting at fromIdx by bias on the right. If necessary, first resizes the array. Does not
    // modifies the elements in range [fromIdx; fromIdx + bias). Changes the size of the array by bias
    protected void probeItemsRight(int fromIdx, int bias)
    {
        if (isIndexValid(fromIdx))
        {
            if (bias > 0)
            {
                int newSize = size + bias;

                if (newSize > capacity)
                {
                    resize(newSize * resizeFactor);
                }

                for (int i = size - 1; i >= fromIdx; --i)
                {
                    array[i + bias] = array[i];
                }

                size = newSize;
            }
            else if (bias < 0)
            {
                throw new IllegalArgumentException("Probe bias must be a non-negative value");
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }
    // Another, less inefficient, yet, correct implementation
    //    {
    //        if (isIndexValid(fromIdx))
    //        {
    //            int newSize = size + bias;
    //
    //            if (newSize > capacity)
    //            {
    //                resize(newSize * resizeFactor);
    //            }
    //
    //            int biasedIdx = fromIdx + bias;
    //            int originalSize = size;
    //            int lastProbeIdx = size - 1 + bias;
    //            for (int i = fromIdx; (i < biasedIdx) && (i < originalSize); ++i)
    //            {
    //                T prevMoveItem = array[i];
    //                int probedItemStartIdx = i + bias;
    //                for (int j = probedItemStartIdx; j <= lastProbeIdx; j += bias)
    //                {
    //                    T curMoveItem = array[j];
    //                    array[j] = prevMoveItem;
    //                    prevMoveItem = curMoveItem;
    //                }
    //            }
    //
    //            size = newSize;
    //        }
    //        else
    //        {
    //            throw new IndexOutOfBoundsException();
    //        }
    //    }

    // Moves all items starting at fromIdx on the left until toIdx. Changes the size of the array by (fromIdx - toIdx)
    protected void probeItemsLeft(int toIdx, int fromIdx)
    {
        if (isRangeValid(toIdx, fromIdx))
        {
            if (toIdx != fromIdx)
            {
                for (int i = fromIdx, shiftIdx = toIdx; i < size; ++i, ++shiftIdx)
                {
                    array[shiftIdx] = array[i];
                }

                size -= (fromIdx - toIdx);
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // Actual number of entries in array
    protected int capacity;
    // Array that stores the items of the list
    protected T[] array;
    // Number of items inserted into the list
    protected int size;
    // Multiplier that is used to resize the array. Capacity if multiplied by this factor when resize is needed
    final protected int resizeFactor;

    final static public int DEFAULT_CAPACITY = 50;
    final static public int MIN_CAPACITY = 1;
    final static public int DEFAULT_RESIZE_FACTOR = 2;
    final static public int MIN_RESIZE_FACTOR = 2;
}
