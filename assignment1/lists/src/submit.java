import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main
{
    private static <T> void printList(IList<T> list)
    {
        for (int i = 0; i < list.size(); ++i)
        {
            System.out.println(list.get(i).toString());
        }
    }

    public static void main(String[] args)
    {
        DoubleLinkedList<TournamentStats> tournaments = new DoubleLinkedList<>();
        Scanner input = new Scanner(System.in);

        int numTournaments = input.nextInt();
        input.nextLine();

        for (int i = 0; i < numTournaments; ++i)
        {
            tournaments.addLast(new TournamentStats(input));
        }

        input.close();

        printList(tournaments);
    }
}

// List ADT
// Provides signatures for essential methods of any list
interface IList<T>
{
    // Get size of the list
    int size();
    // True - list has no items, false - list has at least 1 item
    default boolean isEmpty()
    {
        return size() <= 0;
    }

    // Add item to the specified index idx
    void add(int idx, T item);
    // Add item at index 0 (in the front of the array)
    void addFirst(T item);
    // Add item at index size() - 1 (at the end of the array)
    void addLast(T item);

    // Delete item at index idx from the list
    void delete(int idx);
    // Delete the first item from the list
    void deleteFirst();
    // Delete the last item from the list
    void deleteLast();
    // Delete all items from the list
    void clear();
    // Delete the first occurrence of the item from the list
    void deleteFirstItem(T item);
    // Delete all occurrences of the item from the list
    void deleteAllItems(T item);

    // Returns true if item is in the list, otherwise returns false. Comparison is performed via item.equals()
    boolean isItemInList(T item);
    // Returns true if item is in the list, otherwise returns false. Comparison is performed via the comparator
    boolean isItemInList(T item, Comparator<T> comparator);
    // Returns first occurrence of the item, if item is not in the list - returns -1.
    // Comparison is performed via item.equals()
    int findFirst(T item);
    // Returns first occurrence of the item, if item is not in the list - returns -1.
    // Comparison is performed via the comparator
    int findFirst(T item, Comparator<T> comparator);

    // Set item at index idx
    void set(int idx, T item);
    // Get idx-th item of the list
    T get(int idx);
}

interface IExtendedList<T> extends IList<T>
{
    // Returns true when leftIdx & rightIdx are valid indexes and leftIdx is <= rightIdx
    default boolean isRangeValid(int leftIdx, int rightIdx)
    {
        return ((isIndexValid(leftIdx) && isIndexValid(rightIdx)) && (leftIdx <= rightIdx));
    }
    // Returns true if index is valid, that is: strictly less than size and non-negative
    default boolean isIndexValid(int idx)
    {
        return (idx < size()) && (idx >= 0);
    }
    // Returns true when the list is not empty and the idx is last
    default boolean isIndexLast(int idx)
    {
        return (!isEmpty() && (idx == lastIndex()));
    }
    // Returns the index of the last element from the list, that is (size() - 1)
    // If list is empty, returns -1
    default int lastIndex()
    {
        return (size() - 1);
    }
}

// Comparator for any comparable type
class GenericComparator<T extends Comparable<T>> implements Comparator<T>
{
    @Override
    public int compare(T left, T right)
    {
        return left.compareTo(right);
    }
}

enum SortingOrder
{
    ASCENDING,
    DESCENDING
}

// Interface that represents a sortable list of comparable items
// It provides signature and default implementations for sorting lists
// The default implemented algorithm is insertion sort
interface ISortableList<T extends Comparable<T>> extends IList<T>
{
    // Sort data of the implementer in sortOrderOption sort order
    // By default sort order is SortingOrder.ASCENDING
    default void sort(Optional<SortingOrder> sortOrderOption)
    {
        sort(this, new GenericComparator<T>(), sortOrderOption);
    }

    // Sort data of the implementer using comparator in sortOrderOption sort order
    // By default sort order is SortingOrder.ASCENDING
    default void sort(Comparator<T> comparator, Optional<SortingOrder> sortOrderOption)
    {
        sort(this, comparator, sortOrderOption);
    }

    // Static method to sort list using comparator in sortOrderOption.
    // By default sort order is SortingOrder.ASCENDING
    // The implementation is insertion sort
    static <T> void sort(IList<T> list, Comparator<T> comparator, Optional<SortingOrder> sortOrderOption)
    {
        final int compareFactor =
                (sortOrderOption.isPresent() && sortOrderOption.get() == SortingOrder.DESCENDING) ? -1 : 1;

        for (int i = 1; i < list.size(); ++i)
        {
            T curItem = list.get(i);

            int j = i - 1;
            while ((j >= 0) && (compareFactor * comparator.compare(curItem, list.get(j)) < 0))
            {
                list.set(j + 1, list.get(j));
                --j;
            }

            list.set(j + 1, curItem);
        }
    }
}

class ArrayList<T> implements IExtendedList<T>
{
    // Default constructor
    public ArrayList()
    {
        size = 0;
        resizeFactor = DEFAULT_RESIZE_FACTOR;
        capacity = DEFAULT_CAPACITY;
        array = (T[]) new Object[capacity];
    }

    // Constructor with initial capacity of the array
    public ArrayList(int initCapacity)
    {
        if (initCapacity >= MIN_CAPACITY)
        {
            capacity = initCapacity;
        }
        else
        {
            capacity = DEFAULT_CAPACITY;
            throw new IllegalArgumentException("The capacity must be at least. Setting it to default - " +
                    Integer.toString(DEFAULT_CAPACITY));
        }

        size = 0;
        resizeFactor = DEFAULT_RESIZE_FACTOR;
        array = (T[]) new Object[capacity];
    }

    // Constructor with initial capacity of the array and its resize factor
    public ArrayList(int initCapacity, int resizeFactorVal)
    {
        if (resizeFactorVal >= MIN_RESIZE_FACTOR)
        {
            resizeFactor = resizeFactorVal;
        }
        else
        {
            resizeFactor = DEFAULT_RESIZE_FACTOR;
            throw new IllegalArgumentException("The resize factor must be at least 2. Setting it to default - " +
                    Integer.toString(DEFAULT_RESIZE_FACTOR));
        }

        if (initCapacity >= MIN_CAPACITY)
        {
            capacity = initCapacity;
        }
        else
        {
            capacity = DEFAULT_CAPACITY;
            throw new IllegalArgumentException("The capacity must be at least. Setting it to default - " +
                    Integer.toString(DEFAULT_CAPACITY));
        }

        size = 0;
        array = (T[]) new Object[capacity];
    }

    // Returns the value of the resizeFactor
    public int getResizeFactor()
    {
        return resizeFactor;
    }

    // Returns the value of the capacity
    public int getCapacity()
    {
        return capacity;
    }

    @Override
    public int size()
    {
        return size;
    }

    @Override
    public boolean isEmpty()
    {
        return size == 0;
    }

    @Override
    public boolean isRangeValid(int leftIdx, int rightIdx)
    {
        return ((isIndexValid(leftIdx) && isIndexValid(rightIdx)) && (leftIdx <= rightIdx));
    }

    @Override
    public boolean isIndexValid(int idx)
    {
        return (idx < size) && (idx >= 0);
    }

    @Override
    public boolean isIndexLast(int idx)
    {
        return (!isEmpty() && (idx == (size - 1)));
    }

    // Returns true if the array with newCapacity will be able to store all items it already has. False otherwise
    public boolean canResize(int newCapacity)
    {
        return (newCapacity >= size) && (newCapacity >= MIN_CAPACITY);
    }

    // Resizes the actual array capacity to the newCapacity
    public void resize(int newCapacity)
    {
        if (canResize(newCapacity))
        {
            T[] new_array = (T[]) new Object[newCapacity];
            System.arraycopy(array, 0, new_array, 0, array.length);
            array = new_array;
            capacity = newCapacity;
        }
        else
        {
            throw new IllegalArgumentException(
                    "Cannot resize the list. New capacity is less then the size of the list or less then " +
                            MIN_CAPACITY
            );
        }
    }

    @Override
    public void add(int idx, T item)
    {
        if (isIndexValid(idx))
        {
            resizeIfNecessary();
            probeItemsRight(idx, 1);
            array[idx] = item;
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void addFirst(T item)
    {
        resizeIfNecessary();
        probeItemsRight(0, 1);
        array[0] = item;
    }

    @Override
    public void addLast(T item)
    {
        resizeIfNecessary();
        array[size++] = item;
    }

    @Override
    public void delete(int idx)
    {
        if (isIndexValid(idx))
        {
            if (isIndexLast(idx))
            {
                deleteLast();
            }
            else
            {
                probeItemsLeft(idx, idx + 1);
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void deleteFirstItem(T item)
    {
        int deleteIdx = findFirst(item);

        if (isIndexValid(deleteIdx))
        {
            delete(deleteIdx);
        }
    }

    @Override
    public void deleteAllItems(T item)
    {
        int deleteIdx = -1;

        while ( isIndexValid(deleteIdx = findFirst(item)) )
        {
            delete(deleteIdx);
        }
    }

    @Override
    public void deleteFirst()
    {
        if (isEmpty())
        {
            throw new IndexOutOfBoundsException();
        }
        else
        {
            if (size > 1)
            {
                probeItemsLeft(0, 1);
            }
            else
            {
                deleteLast();
            }
        }
    }

    @Override
    public void deleteLast()
    {
        if (isEmpty())
        {
            throw new IndexOutOfBoundsException();
        }
        else
        {
            --size;
        }
    }

    @Override
    public void clear()
    {
        size = 0;
    }

    // Deletes all elements from the list and resizes it to newCapacity
    public void clear(int newCapacity)
    {
        size = 0;
        resize(newCapacity);
    }

    @Override
    public boolean isItemInList(T item)
    {
        return isIndexValid(findFirst(item));
    }

    @Override
    public boolean isItemInList(T item, Comparator<T> comparator)
    {
        return isIndexValid(findFirst(item, comparator));
    }

    @Override
    public int findFirst(T item)
    {
        int resultIdx = -1;

        for (int i = 0; i < size; ++i)
        {
            if (item.equals(array[i]))
            {
                resultIdx = i;
                break;
            }
        }

        return resultIdx;
    }

    @Override
    public int findFirst(T item, Comparator<T> comparator)
    {
        int resultIdx = -1;

        for (int i = 0; i < size; ++i)
        {
            if (comparator.compare(item, array[i]) == 0)
            {
                resultIdx = i;
                break;
            }
        }

        return resultIdx;
    }

    @Override
    public void set(int idx, T item)
    {
        if (isIndexValid(idx))
        {
            array[idx] = item;
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public T get(int idx)
    {
        if (isIndexValid(idx))
        {
            return array[idx];
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // When called checks if the size of the array already reached its capacity.
    // If that is the case extends its capacity
    protected void resizeIfNecessary()
    {
        if (size == capacity)
        {
            resize(capacity * resizeFactor);
        }
    }

    // Moves all items starting at fromIdx by bias on the right. If necessary, first resizes the array. Does not
    // modifies the elements in range [fromIdx; fromIdx + bias). Changes the size of the array by bias
    protected void probeItemsRight(int fromIdx, int bias)
    {
        if (isIndexValid(fromIdx))
        {
            if (bias > 0)
            {
                int newSize = size + bias;

                if (newSize > capacity)
                {
                    resize(newSize * resizeFactor);
                }

                for (int i = size - 1; i >= fromIdx; --i)
                {
                    array[i + bias] = array[i];
                }

                size = newSize;
            }
            else if (bias < 0)
            {
                throw new IllegalArgumentException("Probe bias must be a non-negative value");
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }
    // Another, less inefficient, yet, correct implementation
    //    {
    //        if (isIndexValid(fromIdx))
    //        {
    //            int newSize = size + bias;
    //
    //            if (newSize > capacity)
    //            {
    //                resize(newSize * resizeFactor);
    //            }
    //
    //            int biasedIdx = fromIdx + bias;
    //            int originalSize = size;
    //            int lastProbeIdx = size - 1 + bias;
    //            for (int i = fromIdx; (i < biasedIdx) && (i < originalSize); ++i)
    //            {
    //                T prevMoveItem = array[i];
    //                int probedItemStartIdx = i + bias;
    //                for (int j = probedItemStartIdx; j <= lastProbeIdx; j += bias)
    //                {
    //                    T curMoveItem = array[j];
    //                    array[j] = prevMoveItem;
    //                    prevMoveItem = curMoveItem;
    //                }
    //            }
    //
    //            size = newSize;
    //        }
    //        else
    //        {
    //            throw new IndexOutOfBoundsException();
    //        }
    //    }

    // Moves all items starting at fromIdx on the left until toIdx. Changes the size of the array by (fromIdx - toIdx)
    protected void probeItemsLeft(int toIdx, int fromIdx)
    {
        if (isRangeValid(toIdx, fromIdx))
        {
            if (toIdx != fromIdx)
            {
                for (int i = fromIdx, shiftIdx = toIdx; i < size; ++i, ++shiftIdx)
                {
                    array[shiftIdx] = array[i];
                }

                size -= (fromIdx - toIdx);
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // Actual number of entries in array
    protected int capacity;
    // Array that stores the items of the list
    protected T[] array;
    // Number of items inserted into the list
    protected int size;
    // Multiplier that is used to resize the array. Capacity if multiplied by this factor when resize is needed
    final protected int resizeFactor;

    final static public int DEFAULT_CAPACITY = 50;
    final static public int MIN_CAPACITY = 1;
    final static public int DEFAULT_RESIZE_FACTOR = 2;
    final static public int MIN_RESIZE_FACTOR = 2;
}

class SortableArrayList<T extends Comparable<T>> extends ArrayList<T> implements ISortableList<T>
{
    public SortableArrayList()
    {
        super();
    }

    public SortableArrayList(int initCapacity)
    {
        super(initCapacity);
    }

    public SortableArrayList(int initCapacity, int resizeFactorVal)
    {
        super(initCapacity, resizeFactorVal);
    }
}

// Mutable node of double linked list
class DoubleLinkedListNode<T>
{
    // Constructs an independent (not linked to any other node) node, setting the initial value
    public DoubleLinkedListNode(T initValue)
    {
        value = initValue;
        next = null;
        previous = null;
    }

    // Constructs a node with initial value and next & previous references
    public DoubleLinkedListNode(T initValue, DoubleLinkedListNode<T> initPrevious, DoubleLinkedListNode<T> initNext)
    {
        value = initValue;
        previous = initPrevious;
        next = initNext;
    }

    // Copy constructor
    public DoubleLinkedListNode(DoubleLinkedListNode<T> otherNode)
    {
        value = otherNode.getValue();
        next = otherNode.getNext();
        previous = otherNode.getPrevious();
    }

    public DoubleLinkedListNode<T> getNext()
    {
        return next;
    }

    public DoubleLinkedListNode<T> setNext(DoubleLinkedListNode<T> next)
    {
        this.next = next;
        return this;
    }

    public DoubleLinkedListNode<T> getPrevious()
    {
        return previous;
    }

    public DoubleLinkedListNode<T> setPrevious(DoubleLinkedListNode<T> previous)
    {
        this.previous = previous;
        return this;
    }

    public T getValue()
    {
        return value;
    }

    public DoubleLinkedListNode<T> setValue(T value)
    {
        this.value = value;
        return this;
    }

    // Reference of the next node
    private DoubleLinkedListNode<T> next;
    // Reference of the previous node
    private DoubleLinkedListNode<T> previous;
    // Value stored by the node
    private T value;
}

class DoubleLinkedList<T> implements IExtendedList<T>
{
    // Default constructor: initializes an empty double linked list
    public DoubleLinkedList()
    {
        size = 0;
        head = null;
        tail = null;
        lastAccessedNode = null;
        lastAccessedNodeIdx = -1;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty()
    {
        return head == null;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void add(int idx, T item)
    {
        if (isIndexValid(idx))
        {
            if (idx == 0)
            {
                addFirst(item);
            }
            else if (idx == lastIndex())
            {
                addLast(item);
            }
            else // if index is not the first and not the last
            {
                DoubleLinkedListNode<T> oldNodeAtIdx = getNode(idx);
                DoubleLinkedListNode<T> prevNode = oldNodeAtIdx.getPrevious();

                lastAccessedNode = new DoubleLinkedListNode<>(item, prevNode, oldNodeAtIdx);

                prevNode.setNext(lastAccessedNode);
                oldNodeAtIdx.setPrevious(lastAccessedNode);
                ++size;
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void addFirst(T item)
    {
        lastAccessedNode = new DoubleLinkedListNode<>(item, null, head);

        if (tail == null)
        {
            tail = lastAccessedNode;
        }
        else
        {
            head.setPrevious(lastAccessedNode);
        }

        head = lastAccessedNode;

        ++size;
        lastAccessedNodeIdx = 0;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void addLast(T item)
    {
        lastAccessedNode = new DoubleLinkedListNode<>(item, tail, null);

        if (head == null)
        {
            head = lastAccessedNode;
        }
        else
        {
            tail.setNext(lastAccessedNode);
        }

        tail = lastAccessedNode;

        ++size;
        lastAccessedNodeIdx = lastIndex();
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void delete(int idx)
    {
        if (idx == 0)
        {
            deleteFirst();
        }
        else if (idx == lastIndex())
        {
            deleteLast();
        }
        else // if index is not the first and not the last
        {
            DoubleLinkedListNode<T> oldNodeAtIdx = getNode(idx);
            DoubleLinkedListNode<T> prevNode = oldNodeAtIdx.getPrevious();

            lastAccessedNode = oldNodeAtIdx.getNext().setPrevious(prevNode);

            prevNode.setNext(lastAccessedNode);
            --size;
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void deleteFirst()
    {
        if (isEmpty())
        {
            throw new IndexOutOfBoundsException();
        }
        else if (size == 1)
        {
            clear();
        }
        else
        {
            head = head.getNext();
            lastAccessedNode = head;
            lastAccessedNodeIdx = 0;
            --size;
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void deleteLast()
    {
        if (isEmpty())
        {
            throw new IndexOutOfBoundsException();
        }
        else if (size == 1)
        {
            clear();
        }
        else
        {
            tail = tail.getPrevious();
            lastAccessedNode = tail;
            lastAccessedNodeIdx = lastIndex();
            --size;
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void clear()
    {
        head = null;
        tail = null;
        size = 0;
        lastAccessedNode = null;
        lastAccessedNodeIdx = -1;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void deleteFirstItem(T item)
    {
        int foundItemIdx = findFirst(item);

        if (isIndexValid(foundItemIdx))
        {
            delete(foundItemIdx);
        }
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void deleteAllItems(T item)
    {
        int foundItemIdx = findFirst(item);

        while (isIndexValid(foundItemIdx))
        {
            delete(foundItemIdx);
            foundItemIdx = findFirst(item);
        }
    }

    // Reset lastAccessedNode and its index
    public void resetLastAccessedNode()
    {
        lastAccessedNode = tail;
        lastAccessedNodeIdx = lastIndex();
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public boolean isItemInList(T item)
    {
        return findFirstNode(item) != null;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public boolean isItemInList(T item, Comparator<T> comparator)
    {
        return findFirstNode(item, comparator) != null;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public int findFirst(T item)
    {
        return (findFirstNode(item) == null) ? -1 : lastAccessedNodeIdx;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public int findFirst(T item, Comparator<T> comparator)
    {
        return (findFirstNode(item, comparator) == null) ? -1 : lastAccessedNodeIdx;
    }

    // Returns the first node with its value.equals(item),
    // and sets lastAccessedNode to the found node & its lastAccessedNodeIdx
    // If item is not in the list - returns null & not modifies lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> findFirstNode(T item)
    {
        DoubleLinkedListNode<T> resultItem = head;
        int accessedNodeIdx = (isEmpty()) ? -1 : 0;

        while ((resultItem != null) && !(resultItem.getValue().equals(item)))
        {
            resultItem = resultItem.getNext();
            ++accessedNodeIdx;
        }

        if (resultItem != null)
        {
            lastAccessedNode = resultItem;
            lastAccessedNodeIdx = accessedNodeIdx;
        }

        return resultItem;
    }

    // Returns the first node with its value == item, compared by comparator,
    // and sets lastAccessedNode to the found node & its lastAccessedNodeIdx
    // If item is not in the list - returns null & not modifies lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> findFirstNode(T item, Comparator<T> comparator)
    {
        DoubleLinkedListNode<T> resultItem = head;
        int accessedNodeIdx = (isEmpty()) ? -1 : 0;

        while ((resultItem != null) && (comparator.compare(resultItem.getValue(), item) != 0))
        {
            resultItem = resultItem.getNext();
            ++accessedNodeIdx;
        }

        if (resultItem != null)
        {
            lastAccessedNode = resultItem;
            lastAccessedNodeIdx = accessedNodeIdx;
        }

        return resultItem;
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public void set(int idx, T item)
    {
        getNode(idx).setValue(item);
    }

    // Sets lastAccessedNode & lastAccessedNodeIdx
    @Override
    public T get(int idx)
    {
        return getNode(idx).getValue();
    }

    public int getLastAccessedIdx()
    {
        return lastAccessedNodeIdx;
    }

    // Returns node that is situated at index idx
    // Sets lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> getNode(int idx)
    {
        if (isIndexValid(idx))
        {
            int distanceFromHead = idx;
            int distanceFromLastAccessed = lastAccessedNodeIdx - idx;
            int distanceFromTail = lastIndex() - idx;

            // TODO: fix condition
            if (
                    Math.abs(distanceFromLastAccessed) <= distanceFromHead &&
                    Math.abs(distanceFromLastAccessed) <= distanceFromTail
            )
            // if distance last accessed node is closer to idx, than head and tail
            {
                return traverseFromTo(lastAccessedNode, lastAccessedNodeIdx, idx);
            }
            else if (distanceFromHead <= distanceFromTail)
            {
                return traverseFromTo(head, 0, idx);
            }
            else
            {
                return traverseFromTo(tail, lastIndex(), idx);
            }
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // Based on the position of fromNodeIdx & toIdx, traverses the double linked list in an appropriate direction
    protected DoubleLinkedListNode<T> traverseFromTo(DoubleLinkedListNode<T> fromNode, int fromNodeIdx, int toIdx)
    {
        if (isRangeValid(toIdx, fromNodeIdx))
        {
            return traverseLeftFromTo(fromNode, fromNodeIdx, toIdx);
        }
        else if (isRangeValid(fromNodeIdx, toIdx))
        {
            return traverseRightFromTo(fromNode, fromNodeIdx, toIdx);
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    // Traverses double linked list starting @ fromNode, which index is fromNodeIdx in the list, until toIdx
    // moving in the left direction
    // Sets lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> traverseLeftFromTo(DoubleLinkedListNode<T> fromNode, int fromNodeIdx, int toIdx)
    {
        if (isRangeValid(toIdx, fromNodeIdx))
        {
            lastAccessedNode = fromNode;

            for (lastAccessedNodeIdx = fromNodeIdx; lastAccessedNodeIdx > toIdx; --lastAccessedNodeIdx)
            {
                if (lastAccessedNode != null)
                {
                    lastAccessedNode = lastAccessedNode.getPrevious();
                }
                else
                {
                    resetLastAccessedNode();
                    throw new IllegalArgumentException("Given node index is not the correct index of the node");
                }
            }

            return lastAccessedNode;
        }
        else
        {
            throw new IllegalArgumentException(
                    "Cannot reach index " + toIdx + " when moving from " + fromNodeIdx + " index in the left direction"
            );
        }
    }

    // Traverses double linked list starting @ fromNode, which index is fromNodeIdx in the list, until toIdx
    // moving in the right direction
    // Sets lastAccessedNode & lastAccessedNodeIdx
    protected DoubleLinkedListNode<T> traverseRightFromTo(DoubleLinkedListNode<T> fromNode, int fromNodeIdx, int toIdx)
    {
        if (isRangeValid(fromNodeIdx, toIdx))
        {
            lastAccessedNode = fromNode;

            for (lastAccessedNodeIdx = fromNodeIdx; lastAccessedNodeIdx < toIdx; ++lastAccessedNodeIdx)
            {
                if (lastAccessedNode != null)
                {
                    lastAccessedNode = lastAccessedNode.getNext();
                }
                else
                {
                    resetLastAccessedNode();
                    throw new IllegalArgumentException("Given node index is not the correct index of the node");
                }
            }

            return lastAccessedNode;
        }
        else
        {
            throw new IllegalArgumentException(
                    "Cannot reach index " + toIdx + " when moving from " + fromNodeIdx + " index in the right direction"
            );
        }
    }

    // Number of items inserted into the list
    protected int size;
    // Reference of the first node in the list
    // If no elements in the list is null
    protected DoubleLinkedListNode<T> head;
    // Reference of the last node in the list
    // If no elements in the list is null
    protected DoubleLinkedListNode<T> tail;
    // Reference of the last accessed node to utilize the Principle of Locality
    // If no elements in the list is null
    protected DoubleLinkedListNode<T> lastAccessedNode;
    // Index of the last accessed node
    // If no elements in the list == -1
    protected int lastAccessedNodeIdx;
}

class SortableDoubleLinkedList<T extends Comparable<T>> extends DoubleLinkedList<T> implements ISortableList<T>
{
    public SortableDoubleLinkedList()
    {
        super();
    }
}

class TeamStatsNameComparator implements Comparator<TeamStatisticsEntry>
{
    @Override
    public int compare(TeamStatisticsEntry left, TeamStatisticsEntry right)
    {
        return left.getTeamName().compareTo(right.getTeamName());
    }
}

class TeamStatisticsEntry implements Comparable<TeamStatisticsEntry>
{
    TeamStatisticsEntry(String name)
    {
        this.teamName = name;
        this.gamesPlayed = 0;
        this.wins = 0;
        this.ties = 0;
        this.looses = 0;
        this.goalsScored = 0;
        this.goalsAgainst = 0;
    }

    public TeamStatisticsEntry(String teamName, int gamesPlayed, int wins, int ties, int looses, int goalsScored, int goalsAgainst)
    {
        this.teamName = teamName;
        this.gamesPlayed = gamesPlayed;
        this.wins = wins;
        this.ties = ties;
        this.looses = looses;
        this.goalsScored = goalsScored;
        this.goalsAgainst = goalsAgainst;
    }

    @Override
    public String toString()
    {
        return teamName + " " + (pointsEarned()) + "p, " + gamesPlayed +
                "g (" + wins + "-" + ties + "-" + looses + "), " +
                (goalDifference()) + "gd (" + goalsScored + "-" + goalsAgainst + ")";
    }

    public Integer pointsEarned()
    {
        return (3 * wins) + ties;
    }

    public Integer goalDifference()
    {
        return goalsScored - goalsAgainst;
    }

    @Override
    public int compareTo(TeamStatisticsEntry other)
    {
        int result = pointsEarned().compareTo(other.pointsEarned());

        if (result == 0)
        {
            result = wins.compareTo(other.wins);

            if (result == 0)
            {
                result = goalDifference().compareTo(other.goalDifference());

                if (result == 0)
                {
                    result = teamName.compareTo(other.teamName);
                }
            }
        }

        return result;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLooses() {
        return looses;
    }

    public int getGoalsScored() {
        return goalsScored;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    public void setLooses(int looses) {
        this.looses = looses;
    }

    public void setGoalsScored(int goalsScored) {
        this.goalsScored = goalsScored;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    private String teamName;
    private Integer gamesPlayed;
    private Integer wins, ties, looses;
    private Integer goalsScored, goalsAgainst;
}

class TournamentStats
{
    public TournamentStats(String name, ISortableList<TeamStatisticsEntry> initList)
    {
        tournamentName = name;
        teamsStats = initList;
    }

    // Read tournament
    public TournamentStats(Scanner input)
    {
        tournamentName = input.nextLine();

        int numTeams = input.nextInt();
        input.nextLine();
        teamsStats = new SortableArrayList<>(numTeams);
        for (int i = 0; i < numTeams; ++i)
        {
            teamsStats.addLast(new TeamStatisticsEntry(input.nextLine()));
        }

        int numGames = input.nextInt();
        input.nextLine();
        TeamStatsNameComparator nameComparator = new TeamStatsNameComparator();
        for (int i = 0; i < numGames; ++i)
        {
            String gameEntry = input.nextLine();

            Pattern p = Pattern.compile("^(.+)#(\\d+):(\\d+)#(.+)$");
            Matcher m = p.matcher(gameEntry);
            m.find();

            String leftTeamName = m.group(1);
            String rightTeamName = m.group(4);
            int leftTeamScore = Integer.parseInt(m.group(2));
            int rightTeamScore = Integer.parseInt(m.group(3));

            TeamStatisticsEntry leftTeamStats = teamsStats.get(
                    teamsStats.findFirst(new TeamStatisticsEntry(leftTeamName), nameComparator)
            );
            leftTeamStats.setGamesPlayed(leftTeamStats.getGamesPlayed() + 1);
            leftTeamStats.setGoalsScored(leftTeamStats.getGoalsScored() + leftTeamScore);
            leftTeamStats.setGoalsAgainst(leftTeamStats.getGoalsAgainst() + rightTeamScore);

            TeamStatisticsEntry rightTeamStats = teamsStats.get(
                    teamsStats.findFirst(new TeamStatisticsEntry(rightTeamName), nameComparator)
            );
            rightTeamStats.setGamesPlayed(rightTeamStats.getGamesPlayed() + 1);
            rightTeamStats.setGoalsScored(rightTeamStats.getGoalsScored() + rightTeamScore);
            rightTeamStats.setGoalsAgainst(rightTeamStats.getGoalsAgainst() + leftTeamScore);

            if (leftTeamScore > rightTeamScore)
            {
                leftTeamStats.setWins(leftTeamStats.getWins() + 1);
                rightTeamStats.setLooses(rightTeamStats.getLooses() + 1);
            }
            else if (rightTeamScore > leftTeamScore)
            {
                rightTeamStats.setWins(rightTeamStats.getWins() + 1);
                leftTeamStats.setLooses(leftTeamStats.getLooses() + 1);
            }
            else
            {
                leftTeamStats.setTies(leftTeamStats.getTies() + 1);
                rightTeamStats.setTies(rightTeamStats.getTies() + 1);
            }
        }

        rank();
    }

    // ranks teams
    public void rank()
    {
        teamsStats.sort(Optional.of(SortingOrder.DESCENDING));
    }

    @Override
    public String toString()
    {
        String result = tournamentName + "\n";

        for (int i = 0; i < teamsStats.size(); ++i)
        {
            TeamStatisticsEntry curEntry = teamsStats.get(i);

            result += (i + 1) + ") " +
                    curEntry.toString() + "\n";
        }

        return result;
    }

    private String tournamentName;
    private ISortableList<TeamStatisticsEntry> teamsStats;
}
