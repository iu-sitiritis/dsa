package edu.innopolis;

import java.util.Comparator;

public class TeamStatsNameComparator implements Comparator<TeamStatisticsEntry>
{
    @Override
    public int compare(TeamStatisticsEntry left, TeamStatisticsEntry right)
    {
        return left.getTeamName().compareTo(right.getTeamName());
    }
}
