package edu.innopolis;

import lib.edu.innopolis.*;

import java.util.Scanner;

public class Main
{
    private static <T> void printList(IList<T> list)
    {
        for (int i = 0; i < list.size(); ++i)
        {
            System.out.println(list.get(i).toString());
        }
    }

    public static void main(String[] args)
    {
        DoubleLinkedList<TournamentStats> tournaments = new DoubleLinkedList<>();
        Scanner input = new Scanner(System.in);

        int numTournaments = input.nextInt();
        input.nextLine();

        for (int i = 0; i < numTournaments; ++i)
        {
            tournaments.addLast(new TournamentStats(input));
        }

        input.close();

        printList(tournaments);
    }
}
/* Sample input
2
World Cup 1998 - Group A
4
Brazil
Norway
Morocco
Scotland
6
Brazil#2:1#Scotland
Norway#2:2#Morocco
Scotland#1:1#Norway
Brazil#3:0#Morocco
Morocco#3:0#Scotland
Brazil#1:2#Norway
Some strange tournament
5
Team A
Team B
Team C
Team D
Team E
5
Team A#1:1#Team B
Team A#2:2#Team C
Team A#0:0#Team D
Team E#2:1#Team C
Team E#1:2#Team D
*/
