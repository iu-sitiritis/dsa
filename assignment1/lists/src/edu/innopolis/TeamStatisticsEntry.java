package edu.innopolis;

public class TeamStatisticsEntry implements Comparable<TeamStatisticsEntry>
{
    TeamStatisticsEntry(String name)
    {
        this.teamName = name;
        this.gamesPlayed = 0;
        this.wins = 0;
        this.ties = 0;
        this.looses = 0;
        this.goalsScored = 0;
        this.goalsAgainst = 0;
    }

    public TeamStatisticsEntry(String teamName, int gamesPlayed, int wins, int ties, int looses, int goalsScored, int goalsAgainst)
    {
        this.teamName = teamName;
        this.gamesPlayed = gamesPlayed;
        this.wins = wins;
        this.ties = ties;
        this.looses = looses;
        this.goalsScored = goalsScored;
        this.goalsAgainst = goalsAgainst;
    }

    @Override
    public String toString()
    {
        return teamName + " " + (pointsEarned()) + "p, " + gamesPlayed +
                "g (" + wins + "-" + ties + "-" + looses + "), " +
                (goalDifference()) + "gd (" + goalsScored + "-" + goalsAgainst + ")";
    }

    public Integer pointsEarned()
    {
        return (3 * wins) + ties;
    }

    public Integer goalDifference()
    {
        return goalsScored - goalsAgainst;
    }

    @Override
    public int compareTo(TeamStatisticsEntry other)
    {
        int result = pointsEarned().compareTo(other.pointsEarned());

        if (result == 0)
        {
            result = wins.compareTo(other.wins);

            if (result == 0)
            {
                result = goalDifference().compareTo(other.goalDifference());

                if (result == 0)
                {
                    result = teamName.compareTo(other.teamName);
                }
            }
        }

        return result;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLooses() {
        return looses;
    }

    public int getGoalsScored() {
        return goalsScored;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    public void setLooses(int looses) {
        this.looses = looses;
    }

    public void setGoalsScored(int goalsScored) {
        this.goalsScored = goalsScored;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    private String teamName;
    private Integer gamesPlayed;
    private Integer wins, ties, looses;
    private Integer goalsScored, goalsAgainst;
}
