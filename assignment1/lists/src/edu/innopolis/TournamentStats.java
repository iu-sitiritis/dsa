package edu.innopolis;

import lib.edu.innopolis.ISortableList;
import lib.edu.innopolis.SortableArrayList;
import lib.edu.innopolis.SortingOrder;

import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TournamentStats
{
    public TournamentStats(String name, ISortableList<TeamStatisticsEntry> initList)
    {
        tournamentName = name;
        teamsStats = initList;
    }

    // Read tournament
    public TournamentStats(Scanner input)
    {
        tournamentName = input.nextLine();

        int numTeams = input.nextInt();
        input.nextLine();
        teamsStats = new SortableArrayList<>(numTeams);
        for (int i = 0; i < numTeams; ++i)
        {
            teamsStats.addLast(new TeamStatisticsEntry(input.nextLine()));
        }

        int numGames = input.nextInt();
        input.nextLine();
        TeamStatsNameComparator nameComparator = new TeamStatsNameComparator();
        for (int i = 0; i < numGames; ++i)
        {
            String gameEntry = input.nextLine();

            Pattern p = Pattern.compile("^(.+)#(\\d+):(\\d+)#(.+)$");
            Matcher m = p.matcher(gameEntry);
            m.find();

            String leftTeamName = m.group(1);
            String rightTeamName = m.group(4);
            int leftTeamScore = Integer.parseInt(m.group(2));
            int rightTeamScore = Integer.parseInt(m.group(3));

            TeamStatisticsEntry leftTeamStats = teamsStats.get(
                    teamsStats.findFirst(new TeamStatisticsEntry(leftTeamName), nameComparator)
            );
            leftTeamStats.setGamesPlayed(leftTeamStats.getGamesPlayed() + 1);
            leftTeamStats.setGoalsScored(leftTeamStats.getGoalsScored() + leftTeamScore);
            leftTeamStats.setGoalsAgainst(leftTeamStats.getGoalsAgainst() + rightTeamScore);

            TeamStatisticsEntry rightTeamStats = teamsStats.get(
                    teamsStats.findFirst(new TeamStatisticsEntry(rightTeamName), nameComparator)
            );
            rightTeamStats.setGamesPlayed(rightTeamStats.getGamesPlayed() + 1);
            rightTeamStats.setGoalsScored(rightTeamStats.getGoalsScored() + rightTeamScore);
            rightTeamStats.setGoalsAgainst(rightTeamStats.getGoalsAgainst() + leftTeamScore);

            if (leftTeamScore > rightTeamScore)
            {
                leftTeamStats.setWins(leftTeamStats.getWins() + 1);
                rightTeamStats.setLooses(rightTeamStats.getLooses() + 1);
            }
            else if (rightTeamScore > leftTeamScore)
            {
                rightTeamStats.setWins(rightTeamStats.getWins() + 1);
                leftTeamStats.setLooses(leftTeamStats.getLooses() + 1);
            }
            else
            {
                leftTeamStats.setTies(leftTeamStats.getTies() + 1);
                rightTeamStats.setTies(rightTeamStats.getTies() + 1);
            }
        }

        rank();
    }

    // ranks teams
    public void rank()
    {
        teamsStats.sort(Optional.of(SortingOrder.DESCENDING));
    }

    @Override
    public String toString()
    {
        String result = tournamentName + "\n";

        for (int i = 0; i < teamsStats.size(); ++i)
        {
            TeamStatisticsEntry curEntry = teamsStats.get(i);

            result += (i + 1) + ") " +
                    curEntry.toString() + "\n";
        }

        return result;
    }

    private String tournamentName;
    private ISortableList<TeamStatisticsEntry> teamsStats;
}
