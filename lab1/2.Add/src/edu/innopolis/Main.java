package edu.innopolis;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args) throws FileNotFoundException
    {
        Scanner input = new Scanner(new FileInputStream("input.txt"));
        PrintStream output = new PrintStream(new FileOutputStream("output.txt"));

        output.print(input.nextLong() + input.nextLong());

        input.close();
        output.close();
    }
}
