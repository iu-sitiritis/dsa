package edu.innopolis;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args) throws IOException
    {
        Scanner input = new Scanner(new FileInputStream("input.txt"));
        PrintStream output = new PrintStream(new FileOutputStream("output.txt"));

        int num = input.nextInt();
        for (int i = 1; i < num; ++i)
        {
            if ((num % i) == 0)
            {
                output.print(i);
                output.print(' ');
            }
        }

        input.close();
        output.close();
    }
}
