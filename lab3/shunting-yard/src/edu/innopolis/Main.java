package edu.innopolis;

import java.io.PrintStream;
import java.util.*;

public class Main
{
    private static boolean isOperator(char c)
    {
        return (c == '*') || (c == '/') || (c == '+') || (c == '-');
    }

    private static boolean isLeftAssociative(char c)
    {
        return (c == '*') || (c == '/') || (c == '+') || (c == '-');
    }

    private static HashMap<Character, Integer> precedence = new HashMap<>();

    public static void main(String[] args) throws Exception {
        precedence.put('*', 1);
        precedence.put('/', 1);
        precedence.put('+', 0);
        precedence.put('-', 0);

        Scanner inputScanner = new Scanner(System.in).useDelimiter(" ");
        String input = inputScanner.nextLine();
        inputScanner.close();

        Queue<String> resultQueue = new LinkedList<>();
        Stack<Character> operatorStack = new Stack<>();
        boolean parsingNumber = false;

        String curToken = "";
        for (int i = 0; i < input.length(); ++i)
        {
            char curChar = input.charAt(i);

            if (Character.isDigit(curChar))
            {
                curToken += curChar;
                parsingNumber = true;
            }
            else
            {
                if (parsingNumber)
                {
                    parsingNumber = false;
                    resultQueue.add(curToken);
                }

                if (isOperator(curChar))
                {
                    if (!operatorStack.empty())
                    {
                        char topStack = operatorStack.peek();

                        while (isOperator(topStack) &&
                                (
                                        (
                                            precedence.get(curChar) <= precedence.get(topStack) &&
                                            isLeftAssociative(curChar)
                                        )
                                        ||
                                        (
                                            precedence.get(curChar) < precedence.get(topStack) &&
                                            !isLeftAssociative(curChar)
                                        )
                                )
                        )
                        {
                            resultQueue.add(operatorStack.pop().toString());
                        }
                    }

                    operatorStack.push(curChar);
                }
                else if (curChar == '(')
                {
                    operatorStack.push(curChar);
                }
                else if (curChar == ')')
                {
                    while (operatorStack.peek() != '(')
                    {
                         resultQueue.add(operatorStack.pop().toString());
                    }

                    if (operatorStack.peek() == '(')
                    {
                        operatorStack.pop();
                    }
                    else
                    {
                        throw new Exception("Missmatched parentesis");
                    }
                }

                curToken = "";
            }
        }

        if (parsingNumber)
        {
            parsingNumber = false;
            resultQueue.add(curToken);
        }

        while (!operatorStack.isEmpty())
        {
            if (curToken.equals("(") || curToken.equals(")"))
            {
                throw new Exception("Missmatched parentesis");
            }
            else
            {
                resultQueue.add(operatorStack.pop().toString());
            }
        }

        PrintStream output = new PrintStream(System.out);

        while (!resultQueue.isEmpty())
        {
            output.print(resultQueue.poll() + (resultQueue.isEmpty() ? "" : " "));
        }

        output.close();
    }
}
