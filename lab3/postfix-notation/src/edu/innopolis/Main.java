package edu.innopolis;

import java.io.PrintStream;
import java.util.Scanner;
import java.util.Stack;

public class Main
{
    private static boolean isOperator(String c)
    {
        return (c.equals("*")) || (c.equals("/")) || (c.equals("+")) || (c.equals("-"));
    }

    public static void main(String[] args)
    {
        Scanner inputScanner = new Scanner(System.in);
        String[] input = inputScanner.nextLine().split(" ");
        inputScanner.close();

        Stack<String> postfixExpressionStack = new Stack<>();

        int secondOperand = 1, firstOperand = 0;
        for (String curElement: input)
        {
            if (isOperator(curElement))
            {
                switch (curElement.charAt(0))
                {
                    case '+':
                    {
                        postfixExpressionStack.add(
                                Integer.toString(
                                    Integer.parseInt(postfixExpressionStack.pop())
                                    +
                                    Integer.parseInt(postfixExpressionStack.pop())
                                )
                        );
                        break;
                    }

                    case '-':
                    {
                        secondOperand = Integer.parseInt(postfixExpressionStack.pop());
                        firstOperand = Integer.parseInt(postfixExpressionStack.pop());
                        postfixExpressionStack.add(
                                Integer.toString(firstOperand - secondOperand)
                        );
                        break;
                    }

                    case '*':
                    {
                        postfixExpressionStack.add(
                                Integer.toString(
                                        Integer.parseInt(postfixExpressionStack.pop())
                                        *
                                        Integer.parseInt(postfixExpressionStack.pop())
                                )
                        );
                        break;
                    }

                    case '/':
                    {
                        secondOperand = Integer.parseInt(postfixExpressionStack.pop());
                        firstOperand = Integer.parseInt(postfixExpressionStack.pop());
                        postfixExpressionStack.add(
                                Integer.toString(firstOperand - secondOperand)
                        );
                        break;
                    }
                }
            }
            else
            {
                postfixExpressionStack.add(curElement);
            }
        }

        PrintStream output = new PrintStream(System.out);
        output.print(postfixExpressionStack.pop());
        output.close();
    }
}
