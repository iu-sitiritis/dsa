package edu.innopolis;

import java.awt.*;
import java.awt.geom.Line2D;
import java.io.PrintStream;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        int size = input.nextInt();
        int rndPointX = input.nextInt();
        int numIntersections = 0;
        Point rndPoint = new Point(rndPointX, input.nextInt());
        Line2D.Double ray = new Line2D.Double(rndPoint, new Point(rndPointX, Integer.MAX_VALUE));
        Point startPoint = new Point(input.nextInt(), input.nextInt());
        Point prevPoint = startPoint;

        for (int i = 1; i < size; ++i)
        {
            Point curPoint = new Point(input.nextInt(), input.nextInt());
            Line2D.Double polygonEdge = new Line2D.Double(prevPoint, curPoint);

            if (ray.intersectsLine(polygonEdge))
            {
                ++numIntersections;
            }

            prevPoint = curPoint;
        }

        if (ray.intersectsLine(new Line2D.Double(prevPoint, startPoint)))
        {
            ++numIntersections;
        }

        input.close();

        PrintStream output = new PrintStream(System.out);

        String result = (numIntersections % 2 == 0) ? "NO" : "YES";
        output.print(result);

        output.close();
    }
}
/*
4 1 1
2 0
2 2
0 2
0 0
YES
*/

/*
3 450 450
1 1
0 2
0 1
NO
*/

/*
4 5 5
9 9
9 10
4 10
4 9
NO
*/
