package edu.innopolis;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args) throws Exception
    {
        Scanner inputScanner = new Scanner(System.in);
        String input = inputScanner.nextLine();
        inputScanner.close();

        System.out.print(ExpressionTools.evalPostfixExpression(ExpressionTools.infixToPostfix(input)));
    }
}
