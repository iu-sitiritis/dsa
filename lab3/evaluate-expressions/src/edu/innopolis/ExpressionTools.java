package edu.innopolis;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public final class ExpressionTools
{
    private ExpressionTools() {};

    public static boolean isOperator(char c)
    {
        return (c == '*') || (c == '/') || (c == '+') || (c == '-');
    }

    public static boolean isOperator(String c)
    {
        return (c.equals("*")) || (c.equals("/")) || (c.equals("+")) || (c.equals("-"));
    }

    public static boolean isLeftAssociative(char c)
    {
        return (c == '*') || (c == '/') || (c == '+') || (c == '-');
    }

    public static HashMap<Character, Integer> precedence = new HashMap<>();

    static
    {
        precedence.put('*', 1);
        precedence.put('/', 1);
        precedence.put('+', 0);
        precedence.put('-', 0);
    }

    // Shunting-yard
    public static String infixToPostfix(String infixStr) throws Exception
    {
        Queue<String> resultQueue = new LinkedList<>();
        Stack<Character> operatorStack = new Stack<>();
        boolean parsingNumber = false;

        String curToken = "";
        for (int i = 0; i < infixStr.length(); ++i)
        {
            char curChar = infixStr.charAt(i);

            if (Character.isDigit(curChar))
            {
                curToken += curChar;
                parsingNumber = true;
            }
            else
            {
                if (parsingNumber)
                {
                    parsingNumber = false;
                    resultQueue.add(curToken);
                }

                if (isOperator(curChar))
                {
                    if (!operatorStack.empty())
                    {
                        char topStack = operatorStack.peek();

                        while (isOperator(topStack) &&
                                (
                                        (
                                                precedence.get(curChar) <= precedence.get(topStack) &&
                                                        isLeftAssociative(curChar)
                                        )
                                                ||
                                                (
                                                        precedence.get(curChar) < precedence.get(topStack) &&
                                                                !isLeftAssociative(curChar)
                                                )
                                )
                        )
                        {
                            resultQueue.add(operatorStack.pop().toString());
                        }
                    }

                    operatorStack.push(curChar);
                }
                else if (curChar == '(')
                {
                    operatorStack.push(curChar);
                }
                else if (curChar == ')')
                {
                    while (operatorStack.peek() != '(')
                    {
                        resultQueue.add(operatorStack.pop().toString());
                    }

                    if (operatorStack.peek() == '(')
                    {
                        operatorStack.pop();
                    }
                    else
                    {
                        throw new Exception("Missmatched parentesis");
                    }
                }

                curToken = "";
            }
        }

        if (parsingNumber)
        {
            parsingNumber = false;
            resultQueue.add(curToken);
        }

        while (!operatorStack.isEmpty())
        {
            if (curToken.equals("(") || curToken.equals(")"))
            {
                throw new Exception("Missmatched parentesis");
            }
            else
            {
                resultQueue.add(operatorStack.pop().toString());
            }
        }

        String result = "";

        while (!resultQueue.isEmpty())
        {
            result += resultQueue.poll() + (resultQueue.isEmpty() ? "" : " ");
        }

        return result;
    }

    // Evaluate expression in postfix (Polish reverse notation)
    // All elements in the input must be space-separated
    public static int evalPostfixExpression(String postfixExpression)
    {
        Stack<String> postfixExpressionStack = new Stack<>();

        String[] input = postfixExpression.split(" ");

        int secondOperand = 1, firstOperand = 0;
        for (String curElement: input)
        {
            if (isOperator(curElement))
            {
                switch (curElement.charAt(0))
                {
                    case '+':
                    {
                        postfixExpressionStack.add(
                                Integer.toString(
                                        Integer.parseInt(postfixExpressionStack.pop())
                                                +
                                                Integer.parseInt(postfixExpressionStack.pop())
                                )
                        );
                        break;
                    }

                    case '-':
                    {
                        secondOperand = Integer.parseInt(postfixExpressionStack.pop());
                        firstOperand = Integer.parseInt(postfixExpressionStack.pop());
                        postfixExpressionStack.add(
                                Integer.toString(firstOperand - secondOperand)
                        );
                        break;
                    }

                    case '*':
                    {
                        postfixExpressionStack.add(
                                Integer.toString(
                                        Integer.parseInt(postfixExpressionStack.pop())
                                                *
                                                Integer.parseInt(postfixExpressionStack.pop())
                                )
                        );
                        break;
                    }

                    case '/':
                    {
                        secondOperand = Integer.parseInt(postfixExpressionStack.pop());
                        firstOperand = Integer.parseInt(postfixExpressionStack.pop());
                        postfixExpressionStack.add(
                                Integer.toString(firstOperand - secondOperand)
                        );
                        break;
                    }
                }
            }
            else
            {
                postfixExpressionStack.add(curElement);
            }
        }

        return Integer.parseInt(postfixExpressionStack.pop());
    }
}
